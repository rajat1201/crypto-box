#include "dma.h"
#include <stdint.h>

#define DMA_INTERRUPTS (DMA_CCR_TEIE|DMA_CCR_HTIE|DMA_CCR_TCIE|DMA_CCR_EN)

void waitfor(unsigned int secs) {
	unsigned int time = 0;
	while(time++ < secs);
}

void wait_for_dma_interrupt()
{
	while(((*dma_isr)&0x2)!=0x2) {
    printf("\t Waiting for DMA interrupt ISR value: %08x\n",*dma_isr);
	}
	*dma_ccr2= 0x0;	//disabling the DMA channel
	*dma_ccr1= 0x0;	//disabling the DMA channel
  *dma_ifcr=0xFF; //Clearing the interrupt flag
	return;
}

int main()
{
    //printf("Starting DMA mem to mem transfer\n");

	//int a[10]={0xbabe0, 0xbabe1, 0xbabe2, 0xbabe3, 0xbabe4, 0xbabe5, 0xbabe6, 0xbabe7, 0xbabe8, 0xbabe9};
	//int b[10];
	//int c[15]={0xcafe0, 0xcafe1, 0xcafe2, 0xcafe3, 0xcafe4, 0xcafe5, 0xcafe6, 0xcafe7, 0xcafe8, 0xcafe9, 0xcafea, 0xcafeb, 0xcafec, 0xcafed, 0xcafee};
	//int d[15];
  //uint64_t inp[6]={0x313198a2e0370734, 0x3243f6a8885a308d, 0x313198a2e0370735, 0x4243f6a8885a308d, 0x313198a2e0370736, 0x4343f6a8885a308d};
  //uint64_t outp[6]={1,2,3,4,5,6};
  uint64_t inp[2000];
  uint64_t outp[2000];

  for(int i=0; i<2000; i++){
    if((i&0x1)!=0)
      inp[i] = 0;
    else
      inp[i] = i>>1;
    outp[i] = 0;
  }

  uint64_t* inp_ptr= inp;
  uint64_t* outp_ptr= outp;

  //volatile uint64_t* aes_key=(uint64_t*) 0x00021020;
  //volatile uint8_t* aes_config=(uint8_t*) 0x00021060;
  //volatile uint8_t* aes_status=(uint8_t*) 0x00021061;

  volatile uint64_t* aes_plaintext=(uint64_t*) 0x00021400;
  volatile uint64_t* aes_key=(uint64_t*) 0x00021420;
  volatile uint64_t* aes_iv=(uint64_t*) 0x00021440;
  volatile uint64_t* aes_result=(uint64_t*) 0x00021460;
  volatile uint8_t* aes_mode=(uint8_t*) 0x00021480;
  volatile uint8_t* aes_config=(uint8_t*) 0x00021483;
  volatile uint8_t* aes_encdec=(uint8_t*) 0x00021484;
  volatile uint8_t* aes_status=(uint8_t*) 0x00021485;
  volatile uint8_t* aes_keylen=(uint8_t*) 0x00021486;

  //printf("&I: %08x &O: %08x\n",inp_ptr,outp_ptr);

  //Setup giving inputs to AES
  *aes_key= 0xabf7158809cf4f3c;
  *aes_key= 0x2b7e151628aed2a6;
  *aes_mode=0;                    //ECB
  *aes_iv  =0;
  *aes_iv  =0;
  *aes_encdec=0;                  //Encrypt
  *aes_keylen=1;                  //aes 128

  *aes_config=1;     

  *dma_cndtr2= 1600;                //Size of input in bytes
  *dma_cmar2= inp_ptr;               //Memory address
  printf("%lx\n",*dma_cmar2);
  *dma_cpar2= 0x00021400;         //Peripheral address
  
  //Setup reading output from AES
  *dma_cndtr1= 1600;                //Size of input in bytes
  *dma_cmar1= outp_ptr;           //Memory address
  *dma_cpar1= 0x00021460;         //Peripheral address

  //Setup the interrupt lines for both the channels
  //*dma_cselr=0x80;                //Channel 0 is for can_take_inp, Channel 1 is for outp_ready
  *dma_cselr=0x10;                //Channel 0 is for can_take_inp, Channel 1 is for outp_ready
  
  __asm__("fence\n\t");

  int burst=1;
  printf("HI %lx\n",burst);
  //*dma_ccr3= (DMA_CCR_BURST_LEN(burst)|DMA_CCR_MEM2MEM|DMA_CCR_PL(2)|DMA_CCR_MSIZE(DMA_FOURBYTE)|DMA_CCR_PSIZE(DMA_FOURBYTE)|DMA_CCR_MINC|DMA_CCR_PINC|DMA_CCR_DIR|DMA_INTERRUPTS);
  *dma_ccr1= (DMA_CCR_BURST_LEN(burst)|DMA_CCR_PL(2)|DMA_CCR_MSIZE(DMA_EIGHTBYTE)|DMA_CCR_PSIZE(DMA_EIGHTBYTE)|DMA_CCR_MINC|DMA_READ_FROM_PERIPH|DMA_INTERRUPTS);
  *dma_ccr2= (DMA_CCR_BURST_LEN(burst)|DMA_CCR_PL(1)|DMA_CCR_MSIZE(DMA_EIGHTBYTE)|DMA_CCR_PSIZE(DMA_EIGHTBYTE)|DMA_CCR_MINC|DMA_READ_FROM_MEM|DMA_INTERRUPTS);
  //printf("\t DMA_CCR2 value: %08x\n",*dma_ccr2);
  
  //printf("\t DMA_CCR1 value: %08x\n",*dma_ccr1);

  wait_for_dma_interrupt();
  //printf("\t Clearing Interrupt Flags\n");
  *dma_ifcr=0; 

  *aes_config=0;
  for(int i=0;i<100;i++){
    printf("Output %d is 0x %lx %lx\n", i, outp[(i*2)+1], outp[(i*2)]);
  }
	    
  return 0;
}


void aes_driver_dma(void *buf, void *key, void *iv, int mode, int encdec, int keylen, uint64_t length){
        volatile uint64_t* aes_plaintext=(uint64_t*) 0x00021400;
        volatile uint64_t* aes_key=(uint64_t*) 0x00021420;
        volatile uint64_t* aes_iv=(uint64_t*) 0x00021440;
        volatile uint64_t* aes_result=(uint64_t*) 0x00021460;
        volatile uint8_t* aes_mode=(uint8_t*) 0x00021480;
        volatile uint8_t* aes_config=(uint8_t*) 0x00021483;
        volatile uint8_t* aes_encdec=(uint8_t*) 0x00021484;
        volatile uint8_t* aes_status=(uint8_t*) 0x00021485;
        volatile uint8_t* aes_keylen=(uint8_t*) 0x00021486;
        volatile uint8_t* aes_endian=(uint8_t*) 0x00021487;

        uint64_t *ptr2 = (long unsigned int)iv & ~(long unsigned int)0x7;
        int diff = ((char*)iv - (char*)ptr2)<<3;
        uint64_t temp, temp1, temp2;
        temp1 = *(ptr2+1);
        temp  = *(ptr2+2);
        temp2 = ((*((uint64_t*)(ptr2+1)))>>diff)|(temp<<(64-diff));
        *aes_iv = reverse_inp(temp2);
        temp1 = *(ptr2);
        temp = *(ptr2+1);
        temp2 = (temp1>>diff)|(temp<<(64-diff));
        *aes_iv = reverse_inp(temp2);
        *aes_mode   = mode;
        *aes_encdec = encdec;
        *aes_keylen = keylen;
        uint64_t *ptrkey = (long unsigned int)key & ~(long unsigned int)0x7;
        diff = ((char*)key - (char*)ptrkey)<<3;

        //printf("DIFF %x" , diff);
        if(diff==0){
          *aes_key = reverse_inp(*((uint64_t*)(key+24)));
          *aes_key = reverse_inp(*((uint64_t*)(key+16)));
          *aes_key = reverse_inp(*((uint64_t*)(key+8)));
          *aes_key = reverse_inp(*((uint64_t*)(key)));
        }
        else {
          //printf("\n %lx %lx %lx %lx %lx\n", *(ptrkey+4), *(ptrkey+3), *(ptrkey+2), *(ptrkey+1), *(ptrkey));
          temp1 = *(ptrkey+4);
          temp = *(ptrkey+3);
          temp2 = (temp>>diff)|(temp1<<(64-diff));
          //printf(" %lx", temp2);
          *aes_key = reverse_inp(temp2);
          temp1 = *(ptrkey+3);
          temp = *(ptrkey+2);
          temp2 = (temp>>diff)|(temp1<<(64-diff));
          //printf(" %lx", temp2);
          *aes_key = reverse_inp(temp2);
          temp1 = *(ptrkey+2);
          temp = *(ptrkey+1);
          temp2 = (temp>>diff)|(temp1<<(64-diff));
          //printf(" %lx", temp2);
          *aes_key = reverse_inp(temp2);
          temp1 = *(ptrkey+1);
          temp = *(ptrkey+0);
          temp2 = (temp>>diff)|(temp1<<(64-diff));
          //printf(" %lx", temp2);
          *aes_key = reverse_inp(temp2);
        }

        *aes_endian = 1;
        *aes_config = 1;
        //for(int i=0; i<length; i+=8){
        //  *aes_plaintext = (*((uint64_t*)(buf+i)));
        //}
        *dma_cndtr2= length;                //Size of input in bytes
        *dma_cmar2= buf;               //Memory address
        //printf("%lx\n",*dma_cmar2);
        *dma_cpar2= 0x00021400;         //Peripheral address

        //Setup reading output from AES
        *dma_cndtr1= length;                //Size of input in bytes
        *dma_cmar1= buf;           //Memory address
        *dma_cpar1= 0x00021460;         //Peripheral address

        *dma_cselr=0x10;                //Channel 0 is for can_take_inp, Channel 1 is for outp_ready

        __asm__("fence\n\t");

        int burst=2;
        //printf("HI %lx\n",burst);
        //*dma_ccr3= (DMA_CCR_BURST_LEN(burst)|DMA_CCR_MEM2MEM|DMA_CCR_PL(2)|DMA_CCR_MSIZE(DMA_FOURBYTE)|DMA_CCR_PSIZE(DMA_FOURBYTE)|DMA_CCR_MINC|DMA_CCR_PINC|DMA_CCR_DIR|DMA_INTERRUPTS);
        *dma_ccr1= (DMA_CCR_BURST_LEN(burst)|DMA_CCR_PL(2)|DMA_CCR_MSIZE(DMA_EIGHTBYTE)|DMA_CCR_PSIZE(DMA_EIGHTBYTE)|DMA_CCR_MINC|DMA_READ_FROM_PERIPH|DMA_INTERRUPTS);
        *dma_ccr2= (DMA_CCR_BURST_LEN(burst)|DMA_CCR_PL(1)|DMA_CCR_MSIZE(DMA_EIGHTBYTE)|DMA_CCR_PSIZE(DMA_EIGHTBYTE)|DMA_CCR_MINC|DMA_READ_FROM_MEM|DMA_INTERRUPTS);
        ////printf("\t DMA_CCR2 value: %08x\n",*dma_ccr2);

        ////printf("\t DMA_CCR1 value: %08x\n",*dma_ccr1);

        wait_for_dma_interrupt();
        ////printf("\t Clearing Interrupt Flags\n");
        *dma_ifcr=0;




        *aes_config = 0;
        *aes_endian = 0;
        while((*aes_status & 1) == 0);

        for(int i=0; i<length; i+=8){
          if((i&0xf)==0)
            *((uint64_t*)(buf+i+8)) = reverse_inp(*aes_result);
          else
            *((uint64_t*)(buf+i-8)) = reverse_inp(*aes_result);
        }
        ////printf("\nAES outp: ");
        ////printf("%lx %lx %lx %lx", *((uint64_t*)(buf+0)), *((uint64_t*)(buf+8)), *((uint64_t*)(buf+16)), *((uint64_t*)(buf+24)));
}


