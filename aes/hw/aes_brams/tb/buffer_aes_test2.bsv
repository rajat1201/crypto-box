import aes :: *;
import DefaultValue :: *;
typedef enum {
	Start, IsReady, Ready, Idle, NotRYet
} AES_State_type deriving(Bits, Eq, FShow);


module mkAesTest(Empty);
	Reg#(Bit#(128)) plaintext <- mkReg(128);
	Reg#(Bit#(128)) ciphertext <- mkReg(128);
	Reg#(Bit#(128)) key <- mkReg(128);
	Reg#(Bool) _ready <- mkReg(False);
	Reg#(Bool) _done <- mkReg(False);
	Reg#(AES_State_type) rg_state <- mkReg(Idle);
	AES aes_ <- mkAES;
	Reg#(Bit#(128)) counter <- mkReg(8);

  rule idlerule(rg_state == Idle);
		counter <= 0;
		rg_state <= Start;
	endrule
	rule run(rg_state == Start);
		//if(_ready == False) aes_.encrypt(128'h0, 128'h0, False);
    if(_ready == False) begin
        $display("START");
        if(counter==0) aes_.encrypt(128'h5af3da146f8bd15b82e84a68f7fdec2d, 256'ha, False,Bit128);
        if(counter==1) aes_.encrypt(128'h8f02aa16b501d2726d812d6dc9b17c1c, 256'h1, False,Bit128);
        if(counter==2) aes_.encrypt(128'hf72f318c2881eb281bcc7134d8ddd557, 256'h1, True,Bit128);
        if(counter==3) aes_.encrypt(128'he3d4896c7ef2e55746193887e0f77c5b, 256'ha, True,Bit128);
    end
  	rg_state <= Ready;
		_ready <= False;
	endrule
		rule ret(rg_state == Ready);
		let res <- aes_.ret();	
		if(counter < 50000) begin
			rg_state <= Start;
			counter <= counter + 1;
			$display("%h", res);
      if(counter==3) begin
				$finish(0);
      end
		end
		else begin
			$finish(0);
		end
	endrule
endmodule

