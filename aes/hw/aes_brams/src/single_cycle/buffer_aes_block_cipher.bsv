package buffer_aes_block_cipher;
	import aes :: *;
	import FIFOF :: *;
	export Aes_key_type (..) ;
	export Mode (..) ;
	export buffer_aes_block_cipher ::*;

	
	typedef enum {ECB, CBC, CFB, OFB, CTR} Mode deriving(Bits, Eq, FShow);
	typedef enum { Idle1, Wait1, Block_cipher} Blk_state deriving(Bits,Eq,FShow );
interface AES_block;
		method Action start(Bit#(128) intext, Bit#(256) key, Bit#(128) iv,Bool decrypt, Mode mode, Aes_key_type keylenn);
		method Action keygen_start(Bit#(256) key, Aes_key_type keylenn);
		method Action end_of_text();
		method Action put(Bit#(128) nxt_blk);
		method ActionValue#(Bit#(128)) get();
endinterface
module mkAesBlockCipher(AES_block);
	Reg#(Bit#(128)) out_nxt_blk <- mkReg(0);
	Reg#(Bit#(128)) inp_txt <- mkReg(0);
	Reg#(Bit#(256)) key_ <- mkReg(0);
	FIFOF#(Bit#(128)) input_fifo <- mkFIFOF;
	FIFOF#(Bit#(128)) output_fifo <- mkFIFOF;
	AES#(16) aes_ <- mkAES;
	Reg#(Aes_key_type) keyy_len <- mkReg(Bit128);
	Reg#(Bool) decrypt_ <- mkReg(True);
	Reg#(Bool) blockdone <- mkReg(True);
	Reg#(Mode) mode_ <- mkReg(ECB);
	Reg#(Bit#(128)) counter <- mkReg(0);
	Reg#(Blk_state) state_var <- mkReg(Idle1);
	Reg#(Int#(32)) cl_cycle <- mkReg(0);
	
	rule clk_cycle;
	cl_cycle <= cl_cycle + 1;
	endrule
	
	//wait till the aes finishes its encryption
	rule st_aeswait (aes_.outp_ready() && state_var == Wait1 && !blockdone);
	//$display("Wait1");
	//$display("end :%d",cl_cycle);
		Bit#(128) ou <- aes_.ret();
		if(!decrypt_) begin
			case(mode_)
				ECB: output_fifo.enq(ou);
				CBC: begin
					output_fifo.enq(ou);
					out_nxt_blk <= ou;
				end
				CFB: begin
					ou = inp_txt^ou;
					output_fifo.enq(ou);
                                        out_nxt_blk <= ou;
					//$display("%h\n",ou);
				end
				OFB: begin
					out_nxt_blk <= ou;
					output_fifo.enq(ou^inp_txt);
				end
				CTR: begin 
					output_fifo.enq(ou^inp_txt);
					counter <= counter +1;
				end
			endcase
		end
		else begin
      case(mode_)
              ECB: output_fifo.enq(ou);
              CBC: begin
                      ou = out_nxt_blk^ou;
                      output_fifo.enq(ou);
                      out_nxt_blk <= inp_txt;
              end
              CFB: begin
                      ou = inp_txt^ou;
                      output_fifo.enq(ou);
                      out_nxt_blk <= inp_txt;
              end
              OFB: begin
                      out_nxt_blk <= ou;
                      output_fifo.enq(ou^inp_txt);
              end
              CTR: begin
              	  output_fifo.enq(ou^inp_txt);
              	  counter <= counter +1;
              end
      endcase
    end
    blockdone <= True;
    state_var <= Block_cipher;
	endrule
	
	// rule to do block chiper for rounds other than first one
	rule block_start(state_var == Block_cipher && aes_.can_take_inp());
	//$display("block chipher");
		blockdone <= False;
		Bit#(128) in_text = input_fifo.first();
		//$display("start :%d",cl_cycle);
		inp_txt <= in_text;
		if(!decrypt_) begin
			case(mode_)
				ECB: aes_.encrypt(in_text,key_,decrypt_,keyy_len);
				CBC: begin
					let in= in_text^out_nxt_blk;
					aes_.encrypt(in,key_,decrypt_,keyy_len);
				end
				CFB: aes_.encrypt(out_nxt_blk,key_,decrypt_,keyy_len);
				OFB: aes_.encrypt(out_nxt_blk,key_,decrypt_,keyy_len);
				CTR: aes_.encrypt(counter,key_,decrypt_,keyy_len);
			endcase
		end
		else begin
			case(mode_)
        ECB: aes_.encrypt(in_text,key_,decrypt_,keyy_len);
        CBC: aes_.encrypt(in_text,key_,decrypt_,keyy_len);
        CFB: aes_.encrypt(out_nxt_blk,key_,False,keyy_len);
        OFB: aes_.encrypt(out_nxt_blk,key_,False,keyy_len);
        CTR: aes_.encrypt(counter,key_,False,keyy_len);
      endcase
		end
		input_fifo.deq();
		state_var<=Wait1;
	endrule


	method Action start(Bit#(128) intext, Bit#(256) key, Bit#(128) iv,Bool decrypt, Mode mode, Aes_key_type keylenn) if(state_var == Idle1);
		//$display("start");
		//$display("start :%d",cl_cycle);
		keyy_len <= keylenn;
		blockdone <= False;
		decrypt_ <= decrypt;
		mode_ <= mode;
		inp_txt <= intext;
		key_<= key;	
		if(!decrypt) begin
			case(mode)
        ECB: begin
          aes_.encrypt(intext,key,decrypt,keylenn);
					$display("ECB encrypt");
        end
				CBC: begin
					let in= intext^iv;
					//$display("CBC encrypt");
					aes_.encrypt(in,key,decrypt,keylenn);
				end
				CFB: aes_.encrypt(iv,key,decrypt,keylenn);
				OFB: aes_.encrypt(iv,key,decrypt,keylenn);
				CTR: begin
					counter <= iv;
					aes_.encrypt(iv,key,decrypt,keylenn);
				     end

			endcase
		end
		else begin
			case(mode)
        ECB: aes_.encrypt(intext,key,decrypt,keylenn);
				CBC: begin
					//$display("%CBC");
					aes_.encrypt(intext,key,decrypt,keylenn);
					out_nxt_blk<=iv;
				end
        CFB: aes_.encrypt(iv,key,False,keylenn);
        OFB: aes_.encrypt(iv,key,False,keylenn);
        CTR: begin
					counter <= iv;
					aes_.encrypt(iv,key,False,keylenn);
				end
      endcase
		end
		state_var <= Wait1;
		
	endmethod
	method Action keygen_start(Bit#(256) key, Aes_key_type keylenn);
        aes_.genKeys(key, keylenn);
    endmethod
	method Action put(Bit#(128) nxt_blk) if(state_var!=Idle1);
		input_fifo.enq(nxt_blk);
	endmethod
	method ActionValue#(Bit#(128)) get();// if(output_fifo.notEmpty());
		Bit#(128) out_txt = output_fifo.first();
		output_fifo.deq();
		return out_txt;
  endmethod

	method Action end_of_text() if(!input_fifo.notEmpty() && blockdone);
		$display("end of text");
		state_var<=Idle1;
	endmethod
endmodule
endpackage
