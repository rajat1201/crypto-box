package sha256;

  import sequential_sha_engine :: *;
  import AXI4_Lite_Types::*;
  import AXI4_Lite_Fabric::*;
  import AXI4_Types::*;
  import AXI4_Fabric::*;
  import Semi_FIFOF::*;
  //import RS232_modified::*;
  import GetPut::*;
  import FIFO::*;
  import Clocks::*;
  import BUtils::*;
  import device_common::*;
  
  interface UserInterface#(numeric type addr_width);
  	method ActionValue#(Tuple2#(Bit#(64),Bool)) read_req (Bit#(addr_width) addr, AccessSize size);
  	method ActionValue#(Bool) write_req(Bit#(addr_width) addr, Bit#(64) data);
	  method Bool outp_ready();
    method Bool can_take_inp();
  endinterface
  
  /*
  `define InputText 'b00;
  `define OutputReg 'h80;
  `define ConfigurationRegHigh 'hc0;
  `define StatusReg 'hc1;
  `define ConfigurationRegLow 'hc2;
  */
  `include "sha_parameters.bsv"
  typedef enum {
  	Idle, Compute, Ready
  } SHA_State_type deriving(Bits, Eq, FShow);
  
  module mkuser_sha(UserInterface#(addr_width))
  provisos(Add#(a__, 8, addr_width));
  	Ifc_sha_engine sha <- mksequential_sha_engine;
  	Reg#(Bit#(256)) sha_out <- mkReg(0);
  	Reg#(Bit#(512)) rg_input_text <- mkReg(0);
    Reg#(Bool) rg_start <- mkReg(False);
    Reg#(Bool) rg_outp_ready <- mkReg(False);
    Reg#(Bool) rg_continuePreHash <- mkReg(False);
    Reg#(Bool) rg_endian <- mkReg(False);
  
  	Reg#(Bit#(9)) input_index <- mkReg(0);
  	Reg#(Bit#(8)) output_index <- mkReg(0);
    Reg#(Bit#(256)) ph <- mkReg(0);
  
  	rule rl_start(rg_start && sha.ready);
      //Bit#(256) ph='h6a09e667bb67ae853c6ef372a54ff53a510e527f9b05688c1f83d9ab5be0cd19;
  		sha.input_engine(ph, rg_input_text);
      rg_start<= False;
      rg_outp_ready<= False;
      $display("SHA: PH %h", ph);
      $display("SHA: Sending inputs: %h ",rg_input_text);
  	endrule

  	//rule rl_getOutput(sha.ready && !rg_outp_ready);
  	rule rl_getOutput(sha.ready && !(rg_start));
  		//let lv_sha <- sha_.ret();
      let lv_sha <- sha.output_engine.get;
      $display("SHA: Getting o/p: %h", lv_sha);
      rg_outp_ready<= True;
      rg_start<= False;
      sha_out <= lv_sha;
  	endrule
  	
  	//method ActionValue#(Tuple2#(Bit#(64),Bool)) read_req (Bit#(addr_width) addr, AccessSize size) if(rg_outp_ready);
  	method ActionValue#(Tuple2#(Bit#(64),Bool)) read_req (Bit#(addr_width) addr, AccessSize size);
      $display($time,"\tSHA: Read Req: Addr: %h, %h", addr, sha_out); 
  		if(truncate(addr) == `OutputReg) begin
        if(output_index!=3)
  			  output_index<= output_index+1;
        else
          output_index<=0;
        /*
        if(output_index==0) begin
          $display("SHA: Output LSB %h", sha_out[63:0]);
  				return tuple2(sha_out[63:0], True);
        end
        else begin
          $display("SHA: Output MSB %h", sha_out[127:64]);
          rg_outp_ready<= False;
  				return tuple2(sha_out[127:64], True);
        end
        */
        //$display("SHA: Output data %h", sha_out[64*(output_index+1)-1:64*output_index]);
        if(output_index == 3) begin
          rg_outp_ready<= False;
          rg_start <= False;
          $display("SHA: OUTPUT INDEX 3");
        end
        return tuple2(sha_out[64*(output_index+1)-1:64*output_index], True);
  		end
      else if(truncate(addr) == `StatusReg) begin
        $display("SHA: Status read response %b, %b", rg_outp_ready, rg_continuePreHash);
        Bit#(64) read_resp= duplicate({7'd0, pack(rg_outp_ready&&(!rg_continuePreHash))});
        //Bit#(64) read_resp= duplicate({7'd0, pack(rg_outp_ready)});
  			return tuple2(read_resp, True);
      end
      else begin
  			return tuple2(?, False);
      end
  	endmethod
  
  	method ActionValue#(Bool) write_req(Bit#(addr_width) addr, Bit#(64) data);
      $display($time,"\tSHA: Write Req: Addr: %h Data: %h", addr, data); 
  		Bool lv_success= True;
  		/*else if(truncate(addr) == `KeyReg) begin
  			key_index<= ~key_index;
        if(key_index==0) begin
  				rg_key[63:0] <= data;
          $display("SHA: Key LSB %h", data);
        end
        else begin
  				rg_key[127:64] <= data;
          $display("SHA: Key MSB %h", data);
        end
  		end*/
      if(truncate(addr) == `ConfigurationReg) begin
        //ph <= 'h6a09e667bb67ae853c6ef372a54ff53a510e527f9b05688c1f83d9ab5be0cd19;
        //rg_continuePreHash <= unpack(truncate(data));
        if(data[0]==0) begin
          ph <= 'h6a09e667bb67ae853c6ef372a54ff53a510e527f9b05688c1f83d9ab5be0cd19;
          rg_continuePreHash <= False;
        end
        else
          rg_continuePreHash <= True;
      end
      //else if(truncate(addr) == 'ConfigurationRegLow) begin
      //  rg_continuePreHash <= False;
      //end
  		else if(truncate(addr) == `EndianReg) begin
        if(data[0]==0) 
          rg_endian <= False;
        else
          rg_endian <= True;
      end
  		else if(truncate(addr) == `InputText) begin
        if(input_index!=7)
  			  input_index<= input_index+1;
        else
          input_index<=0;
        //if(input_index==0) begin
  				//rg_input_text[63:0] <= data;
          $display("SHA: Input 64 bits %h", data);
        //end
        //rg_input_text[64*input_index:+64] <= data;
        //rg_input_text[64*(input_index+1)-1:64*input_index] <= data;
       $display("SHA: data %h, %h", input_index, data);
        if(rg_endian==False) begin
          if(input_index == 0)
            rg_input_text[511:448] <= data;
          else if(input_index == 1)
            rg_input_text[447:384] <= data;
          else if(input_index == 2)
            rg_input_text[383:320] <= data;
          else if(input_index == 3)
            rg_input_text[319:256] <= data;
          else if(input_index == 4)
            rg_input_text[255:192] <= data;
          else if(input_index == 5)
            rg_input_text[191:128] <= data;
          else if(input_index == 6)
            rg_input_text[127:64] <= data;
          else if(input_index == 7) begin
  			  	rg_input_text[63:0] <= data;
            rg_start <= True;
            if(rg_continuePreHash) begin
              ph <= sha_out;
            end
            else begin
              ph <= 'h6a09e667bb67ae853c6ef372a54ff53a510e527f9b05688c1f83d9ab5be0cd19;
            end
            //$display("SHA: Input LSB %h, PH %h", data, ph);
          end
        end
        else begin
          if(input_index == 0)
            rg_input_text[511:448] <= {data[7:0],data[15:8],data[23:16],data[31:24],data[39:32],data[47:40],data[55:48],data[63:56]};
          else if(input_index == 1)
            rg_input_text[447:384] <= {data[7:0],data[15:8],data[23:16],data[31:24],data[39:32],data[47:40],data[55:48],data[63:56]};
          else if(input_index == 2)
            rg_input_text[383:320] <= {data[7:0],data[15:8],data[23:16],data[31:24],data[39:32],data[47:40],data[55:48],data[63:56]};
          else if(input_index == 3)
            rg_input_text[319:256] <= {data[7:0],data[15:8],data[23:16],data[31:24],data[39:32],data[47:40],data[55:48],data[63:56]};
          else if(input_index == 4)
            rg_input_text[255:192] <= {data[7:0],data[15:8],data[23:16],data[31:24],data[39:32],data[47:40],data[55:48],data[63:56]};
          else if(input_index == 5)
            rg_input_text[191:128] <= {data[7:0],data[15:8],data[23:16],data[31:24],data[39:32],data[47:40],data[55:48],data[63:56]};
          else if(input_index == 6)
            rg_input_text[127:64] <= {data[7:0],data[15:8],data[23:16],data[31:24],data[39:32],data[47:40],data[55:48],data[63:56]};
          else if(input_index == 7) begin
  			  	rg_input_text[63:0] <= {data[7:0],data[15:8],data[23:16],data[31:24],data[39:32],data[47:40],data[55:48],data[63:56]};
            rg_start <= True;
            if(rg_continuePreHash) begin
              ph <= sha_out;
            end
            else begin
              ph <= 'h6a09e667bb67ae853c6ef372a54ff53a510e527f9b05688c1f83d9ab5be0cd19;
            end
            //$display("SHA: Input LSB %h, PH %h", data, ph);
          end
        end

        /*if(input_index == 0)
          rg_input_text[63:0] <= data;
        else if(input_index == 1)
          rg_input_text[127:64] <= data;
        else if(input_index == 2)
          rg_input_text[191:128] <= data;
        else if(input_index == 3)
          rg_input_text[255:192] <= data;
        else if(input_index == 4)
          rg_input_text[319:256] <= data;
        else if(input_index == 5)
          rg_input_text[383:320] <= data;
        else if(input_index == 6)
          rg_input_text[447:384] <= data;
        else if(input_index == 7) begin
  				rg_input_text[511:448] <= data;
          rg_start <= True;
          if(rg_continuePreHash) begin
            ph <= sha_out;
          end
          else begin
            ph <= 'h6a09e667bb67ae853c6ef372a54ff53a510e527f9b05688c1f83d9ab5be0cd19;
          end
          $display("SHA: Input MSB %h", data);
        end*/
  		end
  		else
  			lv_success= False;
  
  		return lv_success;
  	endmethod
  
  	//method can_take_inp= (sha.ready && !rg_outp_ready);
  	method Bool can_take_inp= rg_outp_ready;
  	method outp_ready= rg_outp_ready;
  endmodule
  
  interface Ifc_sha_axi4lite#(numeric type addr_width, 
                              numeric type data_width, 
                              numeric type user_width);
  	interface AXI4_Lite_Slave_IFC#(addr_width, data_width, user_width) slave; 
  	method Bool can_take_inp;
  	method Bool outp_ready;
 	endinterface
  
  module mksha_axi4lite#(Clock sha_clock, Reset sha_reset)
  															(Ifc_sha_axi4lite#(addr_width,data_width,user_width))
  // same provisos for the sha
      provisos( Add#(data_width,0,64),
                Add#(a__, 8, addr_width)
        );
  
  	
  	Clock core_clock<-exposeCurrentClock;
  	Reset core_reset<-exposeCurrentReset;
  	Bool sync_required=(core_clock!=sha_clock);
  	AXI4_Lite_Slave_Xactor_IFC#(addr_width,data_width,user_width)  s_xactor <- mkAXI4_Lite_Slave_Xactor();
  
  	if(!sync_required)begin // If sha is clocked by core-clock.
  		UserInterface#(addr_width) sha<- mkuser_sha();
  		//UserInterface#(addr_width) sha<- mkuser_sha(clocked_by sha_clock, 
      //                                                             reset_by sha_reset, baudrate);
  		//capturing the read requests
  		rule capture_read_request;
  			let rd_req <- pop_o (s_xactor.o_rd_addr);
  			let {rdata,succ} <- sha.read_req(rd_req.araddr,unpack(rd_req.arsize));
  			let lv_resp= AXI4_Lite_Rd_Data {rresp:succ?AXI4_LITE_OKAY:AXI4_LITE_SLVERR, 
      	                                                      rdata: rdata, ruser: ?}; //TODO user?
  			s_xactor.i_rd_data.enq(lv_resp);//sending back the response
  		endrule              
  
  		// capturing write requests
  		rule capture_write_request;
  			let wr_req  <- pop_o(s_xactor.o_wr_addr);
  			let wr_data <- pop_o(s_xactor.o_wr_data);
  			let succ <- sha.write_req(wr_req.awaddr,wr_data.wdata);
      		let lv_resp = AXI4_Lite_Wr_Resp {bresp: succ?AXI4_LITE_OKAY:AXI4_LITE_SLVERR, buser: ?};
      		s_xactor.i_wr_resp.enq(lv_resp);//enqueuing the write response
  		endrule
  		interface slave = s_xactor.axi_side;
  	  method Bool can_take_inp= sha.can_take_inp;
  	  method outp_ready= sha.outp_ready;
  	end
  	else begin // if core clock and sha_clock is different.
  		UserInterface#(addr_width) sha<- mkuser_sha;
  		SyncFIFOIfc#(AXI4_Lite_Rd_Addr#(addr_width,user_width)) ff_rd_request <- 
  															mkSyncFIFOFromCC(3,sha_clock);
  		SyncFIFOIfc#(AXI4_Lite_Wr_Addr#(addr_width,user_width)) ff_wr_request <- 
  															mkSyncFIFOFromCC(3,sha_clock);
  		SyncFIFOIfc#(AXI4_Lite_Wr_Data#(data_width)) ff_wdata_request <- mkSyncFIFOFromCC(3,sha_clock);
  		SyncFIFOIfc#(AXI4_Lite_Rd_Data#(data_width,user_width)) ff_rd_response <- 
  															mkSyncFIFOToCC(3,sha_clock,sha_reset);
  		SyncFIFOIfc#(AXI4_Lite_Wr_Resp#(user_width)) ff_wr_response <- 
  															mkSyncFIFOToCC(3,sha_clock,sha_reset);
  		//capturing the read requests
  		rule capture_read_request;
  			let rd_req <- pop_o (s_xactor.o_rd_addr);
  			ff_rd_request.enq(rd_req);
  		endrule
  
  		rule perform_read;
  			let rd_req = ff_rd_request.first;
  			ff_rd_request.deq;
  			let {rdata,succ} <- sha.read_req(rd_req.araddr,unpack(rd_req.arsize));
  			let lv_resp= AXI4_Lite_Rd_Data {rresp:succ?AXI4_LITE_OKAY:AXI4_LITE_SLVERR, 
      	                                                      rdata: rdata, ruser: ?}; //TODO user?
  			ff_rd_response.enq(lv_resp);
  		endrule
  
  		rule send_read_response;
  			ff_rd_response.deq;
  			s_xactor.i_rd_data.enq(ff_rd_response.first);//sending back the response
  		endrule              
  
  		// capturing write requests
  		rule capture_write_request;
  			let wr_req  <- pop_o(s_xactor.o_wr_addr);
  			let wr_data <- pop_o(s_xactor.o_wr_data);
  			ff_wr_request.enq(wr_req);
  			ff_wdata_request.enq(wr_data);
  		endrule
  
  		rule perform_write;
  			let wr_req  = ff_wr_request.first;
  			let wr_data = ff_wdata_request.first;
  			let succ <- sha.write_req(wr_req.awaddr,wr_data.wdata);
      		let lv_resp = AXI4_Lite_Wr_Resp {bresp: succ?AXI4_LITE_OKAY:AXI4_LITE_SLVERR, buser: ?};
  			ff_wr_response.enq(lv_resp);
  		endrule
  
  		rule send_write_response;
  			ff_wr_response.deq;
      		s_xactor.i_wr_resp.enq(ff_wr_response.first);//enqueuing the write response
  		endrule
  		interface slave = s_xactor.axi_side;
  	  method Bool can_take_inp= sha.can_take_inp;
  	  method outp_ready= sha.outp_ready;
  	end
  endmodule:mksha_axi4lite
  
  interface Ifc_sha_axi4#(numeric type addr_width, 
                          numeric type data_width, 
                          numeric type user_width);
  	interface AXI4_Slave_IFC#(addr_width, data_width, user_width) slave; 
  	method Bool can_take_inp;
  	method Bool outp_ready;
 	endinterface
  
  module mksha_axi4#(Clock sha_clock, Reset sha_reset)
  															(Ifc_sha_axi4#(addr_width,data_width,user_width))
  // same provisos for the sha
      provisos( Add#(data_width,0,64),
                Add#(a__, 8, addr_width)
        );
  
  	
  	Clock core_clock<-exposeCurrentClock;
  	Reset core_reset<-exposeCurrentReset;
  	Bool sync_required=(core_clock!=sha_clock);
  	AXI4_Slave_Xactor_IFC#(addr_width,data_width,user_width)  s_xactor <- mkAXI4_Slave_Xactor();
  
  	if(!sync_required)begin // If sha is clocked by core-clock.
  		UserInterface#(addr_width) sha<- mkuser_sha();
  		//UserInterface#(addr_width) sha<- mkuser_sha(clocked_by sha_clock, 
      //                                                             reset_by sha_reset, baudrate);
  		//capturing the read requests
  		rule capture_read_request;
  			let rd_req <- pop_o (s_xactor.o_rd_addr);
  			let {rdata,succ} <- sha.read_req(rd_req.araddr,unpack(truncate(rd_req.arsize)));
  			let lv_resp= AXI4_Rd_Data {rresp:succ?AXI4_OKAY:AXI4_SLVERR, rlast: True, rid: rd_req.arid,
      	                                                      rdata: rdata, ruser: ?}; //TODO user?
  			s_xactor.i_rd_data.enq(lv_resp);//sending back the response
  		endrule              
  
  		// capturing write requests
  		rule capture_write_request;
  			let wr_req  <- pop_o(s_xactor.o_wr_addr);
  			let wr_data <- pop_o(s_xactor.o_wr_data);
  			let succ <- sha.write_req(wr_req.awaddr,wr_data.wdata);
      	let lv_resp = AXI4_Wr_Resp {bresp: succ?AXI4_OKAY:AXI4_SLVERR, buser: ?, bid: wr_req.awid};
      	s_xactor.i_wr_resp.enq(lv_resp);//enqueuing the write response
  		endrule
  		interface slave = s_xactor.axi_side;
  	  method Bool can_take_inp= sha.can_take_inp;
  	  method outp_ready= sha.outp_ready;
  	end
  	else begin // if core clock and sha_clock is different.
  		UserInterface#(addr_width) sha<- mkuser_sha;
  		SyncFIFOIfc#(AXI4_Rd_Addr#(addr_width,user_width)) ff_rd_request <- 
  															mkSyncFIFOFromCC(3,sha_clock);
  		SyncFIFOIfc#(AXI4_Wr_Addr#(addr_width,user_width)) ff_wr_request <- 
  															mkSyncFIFOFromCC(3,sha_clock);
  		SyncFIFOIfc#(AXI4_Wr_Data#(data_width)) ff_wdata_request <- mkSyncFIFOFromCC(3,sha_clock);
  		SyncFIFOIfc#(AXI4_Rd_Data#(data_width,user_width)) ff_rd_response <- 
  											 	mkSyncFIFOToCC(3,sha_clock,sha_reset);
  		SyncFIFOIfc#(AXI4_Wr_Resp#(user_width)) ff_wr_response <- 
  															mkSyncFIFOToCC(3,sha_clock,sha_reset);
  		//capturing the read requests
  		rule capture_read_request;
  			let rd_req <- pop_o (s_xactor.o_rd_addr);
  			ff_rd_request.enq(rd_req);
  		endrule
  
  		rule perform_read;
  			let rd_req = ff_rd_request.first;
  			ff_rd_request.deq;
  			let {rdata,succ} <- sha.read_req(rd_req.araddr,unpack(truncate(rd_req.arsize)));
  			let lv_resp= AXI4_Rd_Data {rresp:succ?AXI4_OKAY:AXI4_SLVERR, rlast: True, rid: rd_req.arid,
      	                                                      rdata: rdata, ruser: ?}; //TODO user?
  			ff_rd_response.enq(lv_resp);
  		endrule
  
  		rule send_read_response;
  			ff_rd_response.deq;
  			s_xactor.i_rd_data.enq(ff_rd_response.first);//sending back the response
  		endrule              
  
  		// capturing write requests
  		rule capture_write_request;
  			let wr_req  <- pop_o(s_xactor.o_wr_addr);
  			let wr_data <- pop_o(s_xactor.o_wr_data);
  			ff_wr_request.enq(wr_req);
  			ff_wdata_request.enq(wr_data);
  		endrule
  
  		rule perform_write;
  			let wr_req  = ff_wr_request.first;
  			let wr_data = ff_wdata_request.first;
  			let succ <- sha.write_req(wr_req.awaddr,wr_data.wdata);
      		let lv_resp = AXI4_Wr_Resp {bresp: succ?AXI4_OKAY:AXI4_SLVERR, buser: ?, bid: wr_req.awid};
  			ff_wr_response.enq(lv_resp);
  		endrule
  
  		rule send_write_response;
  			ff_wr_response.deq;
      		s_xactor.i_wr_resp.enq(ff_wr_response.first);//enqueuing the write response
  		endrule
  		interface slave = s_xactor.axi_side;
  	  method Bool can_take_inp= sha.can_take_inp;
  	  method outp_ready= sha.outp_ready;
  	end
  endmodule:mksha_axi4
endpackage
