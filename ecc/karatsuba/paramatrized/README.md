# Paramatrized Hybrid Karatsuba Multiplier

## Algorithm
### Hybrid Karatsuba multiplier
	Algorithm: hkmul (Hybrid Karatsuba Multiplier)													
		Input: The multiplicands A, B and their length m							
		Output: C of length 2m − 1 bits													

	1 	begin																		
	2 		if m < 29 then															
	3 			return gkmul(A, B, m)												
	4 		else																	
	5 			l = ⌈m/2⌉							//	⌈⌉ : ceiling function		|
	6 			A′ = A[m−1···l] + A[l−1···0]										
	7 			B′ = B[m−1···l] + B[l−1···0]										
	8 			cp1 = hkmul(A[l−1···0], B[l−1···0], l)								
	9 			cp2 = hkmul(A′, B′, l)												
	10 			cp3 = hkmul(A[m−1···l], B[m−1···l], m − l)							
	11 			return (Cp3 << 2l) + (Cp1 + Cp2 + Cp3) << l + Cp1;			// "<<" is left shift operation					
	12 		end																			
	13 	end		
### General Karatsuba multiplier
	Algorithm: gkmul (General Karatsuba Multiplier)
		Input: A, B are multiplicands of m bits
		Output: C of length 2m − 1 bits
		
	/* Define : Mx → AxBx */
	/* Define : M(x,y) → (Ax + Ay)(Bx + By) */
	1 	begin
	2 		for i = 0 to m − 2 do
	3 			Ci = C2m−2−i = 0
	4 			for j = 0 to ⌊i/2⌋ do
	5 				if i = 2j then
	6 					Ci = Ci + Mj
	7 					C2m−2−i = C2m−2−i + Mm−1−j
	8 				else
	9 					Ci = Ci + Mj + Mi−j + M(j,i−j)
	10 					C2m−2−i = C2m−2−i + Mm−1−j
	11 						+Mm−1−i+j + M(m−1−j,m−1−i+j)
	12 				end
	13 			end
	14 		end
	15 		Cm−1 = 0
	16 		for j = 0 to ⌊(m − 1)/2⌋ do
	17 			if m − 1 = 2j then
	18 				Cm−1 = Cm−1 + Mj
	19 			else
	20 				Cm−1 = Cm−1 + Mj + Mm−1−j + M(j,m−1−j)
	21 			end
	22 		end
	23 	end
Source : "ARCHITECTURE EXPLORATIONS FOR ELLIPTIC CURVE CRYPTOGRAPHY ON FPGAS" thesis by Chester Rebeiro, IIT Madras.

## Structure
1) The code is organised into two files "Hkmul.bsv" and "Gkmul.bsv" consisting of paramatrized hybrid karatsuba multiplier and paramatrized general karatsuba multiplier respectively.
2) User only needs to import "Hkmul.bsv".











## Usage: 
1). Can be invoked directly as a function.
	eg:		
 
	rule mult;
		Bit#(233) a = 12345678;
		Bit#(233) b = 91011121;
		Bit#(465) ans = hkmul(a,b);
		$display("%d", ans);
	endrule
							
2). Can be encapsulated into a module using "UniqueWrappers" library.
eg:
  
		import UniqueWrappers::*;	
		import Hkmul::*;
		
		`define size 233
		`define outsize 465		// outsize = (2*size) - 1
		
		module mkTest(Empty);
			Wrapper2#(Bit#(`size), Bit#(`size), Bit#(`outsize)) hmult <- mkUniqueWrapper2(hkmul) ;
			
			Reg#(Bit#(`outsize)) 	res	<- mkReg(0);
			Reg#(Bit#(32)) 			i 	<- mkReg(0);
		
			rule mult(i==0);
				Bit#(`size)a = 13513245;
				Bit#(`size)b = 11311324;
				let ans <- hmult.func(a, b);
				res <= ans;
				i <= i+1;
			endrule;
			
			rule end(i==1);
				$display("%d", res);
			endrule
		
		endmodule
## Note: 
1) User might need to increase elaboration steps limit of bluespec compiler in using **"-steps-max-intervals N"** in some cases if the field size is sufficiently large. 


