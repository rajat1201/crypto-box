package TB;

import Hkmul::*;
//import UniqueWrappers::*;
import StmtFSM::*;

//=================================== SIZE DEFINITION ========================================
`define size 233
`define outsize 465
//============================================================================================


//--------------------------------------------- testbench -------------------------------------
(*synthesize*)
module mktest(Empty);

	// instantiate a unique wrapper module to encapsulate paramatrized hkmult function
	//Wrapper2#(Bit#(`size), Bit#(`size), Bit#(`outsize)) hmult <- mkUniqueWrapper2(hkmul) ;
	
	//Reg#(Bit#(`outsize)) res <- mkReg(0);	// register to store result of multiplication

	Hkmul_ifc hk <- mkHkmul;
	Reg#(Bit#(`outsize)) res <- mkReg(0);
	
	Reg#(Bool) complete <- mkReg(False);
	
	
	Stmt test =
	seq
		$display("----------- Hkmul using UniqueWrappers TESTBENCH --------------");
		
		action
			$display("%d\n", res);
			Bit#(`size)a = 8753124345247658452547;
			Bit#(`size)b = 452348611234565758976035357457658356879;
			let ans <- hk.mul(a,b);
			res <= ans;
		endaction
		action
			$display("%d\n", res);
			Bit#(`size)a = 6578234413426584769803527457675679603;		
			Bit#(`size)b = 8956233567587878908032567245875897425474265815134563463134646725470;	
			let ans <- hk.mul(a,b);
			res <= ans;
		endaction
		action
			$display("%d\n", res);
			Bit#(`size)a = 24234534487890870749307765787695;		
			Bit#(`size)b = 23424532453253262456275790876367822541;
			let ans <- hk.mul(a,b);
			res <= ans;
		endaction
		action
			$display("%d\n", res);
			Bit#(`size)a = 688768707345121212105458854586870749307765787695765864702456245656787;		
			Bit#(`size)b = 1534870687684483684384682452671571348797317845719976;
			let ans <- hk.mul(a,b);
			res <= ans;
		endaction
		action
			$display("%d\n", res);
			Bit#(`size)a = 198465245588545346448186415468411416452167891524684684542046847173548;		
			Bit#(`size)b = 687167175643137148030687076145348644038404806549865464681327134563415;
			let ans <- hk.mul(a,b);
			res <= ans;
		endaction


		$display("ans: %d",res);
		
		$finish;
	endseq;	
	FSM testFSM <- mkFSM (test);


	rule startit ;
		testFSM.start();
	endrule
	
endmodule

endpackage
