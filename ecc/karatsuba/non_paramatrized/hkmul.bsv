//====================================== Hybrid Karatsuba Multiplier =====================================

package hkmul;
import Gkmul::*;


//---------------------------------------- 233 bit --------------------------------

function Bit#(465) hkmul233(Bit#(233) a, Bit#(233) b);
	Integer n = 233;
	
	Bit#(465) ans = ?;
	
	Integer l = n / 2 + 1;						// l = ceil(sz/2)


	Bit#(l)	aDash = a[n-1:l] ^ a[l-2:0];       // working
	aDash[l-1] = a[l-1];

	Bit#(l)	bDash = b[n-1:l] ^ b[l-2:0];		// working
	bDash[l-1] = b[l-1];

		
	Bit#(233)	cp1 	= hkmul117(a[l-1:0], b[l-1:0]); 	// low mul
	Bit#(233) 	cp2 	= hkmul117(aDash, bDash);			// center mul
	Bit#(231) 	cp3 	= hkmul116(a[n-1:l], b[n-1:l]);	// high mul

	// shifting and adding			
	Bit#(465) cP1 = zeroExtend(unpack(cp1));
	Bit#(465) cP2 = zeroExtend(unpack(cp2));
	Bit#(465) cP3 = zeroExtend(unpack(cp3));
	
	ans = ( cP3<<(2*l) ) ^ ( (cP1 ^ cP2 ^ cP3)<<l ) ^ cP1;
	return ans;
endfunction

//---------------------------------------- 117 bit --------------------------------


function Bit#(233) hkmul117(Bit#(117) a, Bit#(117) b);

	Integer n = 117;

	Bit#(233) ans = ?;

	Integer l = n / 2 + 1;						// l = ceil(sz/2)


	Bit#(l)	aDash = a[n-1:l] ^ a[l-2:0];       // working
	aDash[l-1] = a[l-1];

	Bit#(l)	bDash = b[n-1:l] ^ b[l-2:0];		// working
	bDash[l-1] = b[l-1];

		
	Bit#(117)	cp1 	= hkmul59(a[l-1:0], b[l-1:0]); 	// low mul
	Bit#(117) 	cp2 	= hkmul59(aDash, bDash);			// center mul
	Bit#(115) cp3 	= hkmul58(a[n-1:l], b[n-1:l]);	// high mul

	// shifting and adding			
	Bit#(233) cP1 = zeroExtend(unpack(cp1));
	Bit#(233) cP2 = zeroExtend(unpack(cp2));
	Bit#(233) cP3 = zeroExtend(unpack(cp3));

	ans = ( cP3<<(2*l) ) ^ ( (cP1 ^ cP2 ^ cP3)<<l ) ^ cP1;
	return ans;
endfunction

//---------------------------------------- 116 bit --------------------------------
function Bit#(231) hkmul116(Bit#(116) a, Bit#(116) b);

	Integer n = 116;
	
	Bit#(231) ans = ?;

	Integer l = n / 2;						// l = ceil(sz/2)


	Bit#(l)	aDash = a[n-1:l] ^ a[l-1:0];       // working

	Bit#(l)	bDash = b[n-1:l] ^ b[l-1:0];		// working

		
	Bit#(115)		cp1 	= hkmul58(a[l-1:0], b[l-1:0]); 	// low mul
	Bit#(115) 		cp2 	= hkmul58(aDash, bDash);			// center mul
	Bit#(115)	 	cp3 	= hkmul58(a[n-1:l], b[n-1:l]);	// high mul

	// shifting and adding			
	Bit#(231) cP1 = zeroExtend(unpack(cp1));
	Bit#(231) cP2 = zeroExtend(unpack(cp2));
	Bit#(231) cP3 = zeroExtend(unpack(cp3));

	ans = ( cP3<<(2*l) ) ^ ( (cP1 ^ cP2 ^ cP3)<<l ) ^ cP1;
	return ans;
endfunction
//---------------------------------------- 59 bit ---------------------------------

function Bit#(117) hkmul59(Bit#(59) a, Bit#(59) b);

	Integer n = 59;
	
	Bit#(117) ans = ?;

	Integer l = n / 2 + 1;						// l = ceil(sz/2)


	Bit#(l)	aDash = a[n-1:l] ^ a[l-2:0];       // working
	aDash[l-1] = a[l-1];

	Bit#(l)	bDash = b[n-1:l] ^ b[l-2:0];		// working
	bDash[l-1] = b[l-1];

		
	Bit#(59)	cp1 	= hkmul30(a[l-1:0], b[l-1:0]); 	// low mul
	Bit#(59) 	cp2 	= hkmul30(aDash, bDash);			// center mul
	Bit#(57) 	cp3 	= hkmul29(a[n-1:l], b[n-1:l]);	// high mul

	// shifting and adding			
	Bit#(117) cP1 = zeroExtend(unpack(cp1));
	Bit#(117) cP2 = zeroExtend(unpack(cp2));
	Bit#(117) cP3 = zeroExtend(unpack(cp3));

	ans = ( cP3<<(2*l) ) ^ ( (cP1 ^ cP2 ^ cP3)<<l ) ^ cP1;
	
	return ans;
endfunction




//---------------------------------------- 58 bit ---------------------------------

function Bit#(115) hkmul58(Bit#(58) a, Bit#(58) b);

	Integer n = 58;
	
	Bit#(115) ans = ?;

	Integer l = n / 2;						// l = ceil(sz/2)


	Bit#(l)	aDash = a[n-1:l] ^ a[l-1:0];       // working

	Bit#(l)	bDash = b[n-1:l] ^ b[l-1:0];		// working

		
	Bit#(57)		cp1 	= hkmul29(a[l-1:0], b[l-1:0]); 	// low mul
	Bit#(57) 		cp2 	= hkmul29(aDash, bDash);			// center mul
	Bit#(57)	 	cp3 	= hkmul29(a[n-1:l], b[n-1:l]);	// high mul

	// shifting and adding			
	Bit#(115) cP1 = zeroExtend(unpack(cp1));
	Bit#(115) cP2 = zeroExtend(unpack(cp2));
	Bit#(115) cP3 = zeroExtend(unpack(cp3));

	ans = ( cP3<<(2*l) ) ^ ( (cP1 ^ cP2 ^ cP3)<<l ) ^ cP1;

	return ans;
endfunction


//---------------------------------------- 30 bit ---------------------------------
function Bit#(59) hkmul30(Bit#(30) a, Bit#(30) b);

	Integer n = 30;

	Bit#(59) ans = ?;

	Integer l = n / 2;						// l = ceil(sz/2)


	Bit#(l)	aDash = a[n-1:l] ^ a[l-1:0];       // working

	Bit#(l)	bDash = b[n-1:l] ^ b[l-1:0];		// working

		
	Bit#(29)		cp1 	= gkmul15(a[l-1:0], b[l-1:0]); 	// low mul
	Bit#(29) 		cp2 	= gkmul15(aDash, bDash);			// center mul
	Bit#(29) 	cp3 	= gkmul15(a[n-1:l], b[n-1:l]);	// high mul

	// shifting and adding			
	Bit#(59) cP1 = zeroExtend(unpack(cp1));
	Bit#(59) cP2 = zeroExtend(unpack(cp2));
	Bit#(59) cP3 = zeroExtend(unpack(cp3));

	ans = ( cP3<<(2*l) ) ^ ( (cP1 ^ cP2 ^ cP3)<<l ) ^ cP1;

	return ans;
endfunction



//---------------------------------------- 29 bit ---------------------------------

function Bit#(57) hkmul29(Bit#(29) a, Bit#(29) b);

	Integer n = 29;
	
	Bit#(57) ans = ?;

	Integer l = n / 2 + 1;						// l = ceil(sz/2)


	Bit#(l)	aDash = a[n-1:l] ^ a[l-2:0];       // working
	aDash[l-1] = a[l-1];

	Bit#(l)	bDash = b[n-1:l] ^ b[l-2:0];		// working
	bDash[l-1] = b[l-1];

		
	Bit#(29)	cp1 	= gkmul15(a[l-1:0], b[l-1:0]); 	// low mul
	Bit#(29) 	cp2 	= gkmul15(aDash, bDash);			// center mul
	Bit#(27) 	cp3 	= gkmul14(a[n-1:l], b[n-1:l]);	// high mul

	// shifting and adding			
	Bit#(57) cP1 = zeroExtend(unpack(cp1));
	Bit#(57) cP2 = zeroExtend(unpack(cp2));
	Bit#(57) cP3 = zeroExtend(unpack(cp3));

	ans = ( cP3<<(2*l) ) ^ ( (cP1 ^ cP2 ^ cP3)<<l ) ^ cP1;

	return ans;
endfunction


//=================================================================================

module mkTest(Empty);
	
	rule go;
		Bit#(233)a = 233'h65445654bafff45654a87678876c45654653442453254354375476546;
		Bit#(233)b = 233'h476546548978fabc678b876c678976f56765ac567865bc654ca567fac;
		
		Bit#(465)ans = hkmul233(a, b);
		$display(" a: %b %d\n b: %b %d \n\n ans: %b %d",a, a,b, b,ans, ans);
		$finish(0);
	endrule
endmodule

endpackage
