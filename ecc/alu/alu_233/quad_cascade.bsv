//-----------------------QUAD CASCADE BLOCK with 14 QUAD BLOCKS FOR ALU----------------------------------
package quad_cascade;
import quadgen3 :: *;
import Vector ::*;

interface Quadcas_ifc;
method Action start2(Bit#(233) a,bit[3:0] q_sel);// input for quad cascade block and control signal to choose the quad power
method Bit#(233) result2();

endinterface: Quadcas_ifc

module mk_quadcas(Quadcas_ifc);





Reg# (int) i2<-mkReg(0);


Reg# (bit[3:0]) q<-mkReg(0);

Reg# (int) s<-mkReg(0);
Vector#(14 ,Quad_ifc) x;                // instantiating 14 quad blocks
for(Integer i3=0;i3<14;i3=i3+1)  
	x[i3]<-mk_quad;

rule cycle(s==1 && i2<15 );           
Bit#(233) in = 0;
in=(x[i2-1].result()); //receiving the output of previous quad block

x[i2].start(in); // sending the receivied output to next quad block
i2<=i2+1;
if(i2==14)
	in=(x[i2].result()); 
if(i2==14)
	s<=3;   
endrule


method Action start2(a,q_sel);


s<=1;
q<=q_sel-1;
x[0].start(a);  // giving the input to first quad block
i2<=1;
endmethod

method result2()if(s==3);            // required output is selected using q_sel

return(x[q].result());

endmethod


endmodule:mk_quadcas
 
module mktestcas();//testbench
Reg#(int) state<-mkReg(0);
Quadcas_ifc mt<-mk_quadcas;

rule go(state==0);
mt.start2(233'b11100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
 ,4'b0011); // (input , quad power as control signal) 
state<=2;
endrule

rule finish(state==2);
$display("output=%b",mt.result2()); // output of quad cascade block

state<=4;
endrule
rule go2(state==4);
mt.start2( 233'b01010100000000000000000000000000000000000000000000000000000000000000000000000000000000101010000000000000000000000000000000000000000000000000000000000000000000001010100000000000000000000000000000000000000000000000000000000000000000000
,4'b0011);         // change the input according to the field
state<=7;
endrule

rule finish2(state==7);
$display("Quad=%b",mt.result2()); // output is displayed in  bits.

state<=8;
endrule
endmodule:mktestcas

endpackage:quad_cascade
