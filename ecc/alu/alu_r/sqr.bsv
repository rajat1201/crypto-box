package sqr;

`include "details.bsv"
interface Sqr_ifc;
method Action start(bit[1000:0] a);//input to square block


method bit[`nl:0] result();
method int count();
endinterface: Sqr_ifc

module mk_sqr(Sqr_ifc);
Reg# (bit[1000:0]) b<-mkReg(0);



Reg# (int) s<-mkReg(0);
Reg# (int) ct<-mkReg(0);

       



rule cyclce(s==1); 
bit[1500:0] x=0;
for(Integer i=0;i<`n;i=i+1) //adds zeros between each bit of input
	x[2*i]=b[i];
bit[`nl:0] x1 =(x[(`n*2):((`nl*2)+2-`m1)])^(x[(`n*2):((`nl*2)+2-`m2)])^(x[(`n*2):((`nl*2)+2-`m3)]);// performs modulo reduction
x1 = x1^(x[`nl:0])^(x[(`nl*2):`n])^(x[(`nl*2):`n]<<`m1)^(x[(`nl*2):`n]<<`m2)^(x[(`nl*2):`n]<<`m3); // suare is performed

b<=zeroExtend(x1);
s<=4;
ct<=ct+1;
endrule


method Action start(a);



b<=a;
s<=1;

endmethod


method result()if(s==4); // output of square block
return (b[`nl:0]); 
endmethod
method count()if(s==4); // returns the clock cyles
return (ct); 
endmethod

endmodule:mk_sqr 

module mktest(); //testbench
Reg#(int) state<-mkReg(0);
Sqr_ifc m<-mk_sqr;

rule go(state==0);
m.start(zeroExtend(571'b1110000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
 ));
state<=2;
endrule

rule finish(state==2);
$display("Quad=%b",m.result()); // output is displayed in  bits.
$display("clock cycles=%d",m.count()); // clock cylecs is displayed in  bits.
state<=4;
endrule

endmodule:mktest

endpackage:sqr
