package ALU;
import quad_cascade ::*;
import sqr::*;
import hkmul::*;
import Vector::*;
`include "details.bsv"    // change the current field parameters in details.bsv file ( n <-degree of the field, nl <-n-1, m1 m2 &m3 are the degrees of the polynomial)

interface ALU_ifc;
method Action start(bit[1000:0] a_0,bit[1000:0] a_1,bit[1000:0] a_2,bit[1000:0] a_3,bit[1000:0] q_in,bit[29:0]cn);
method bit[`nl:0] qout();
method bit[`nl:0] c0();
method bit[`nl:0] c1();
method bit status();

endinterface: ALU_ifc

module mk_ALU(ALU_ifc);
Reg# (bit[1000:0]) a0<-mkReg(0);
Reg# (bit[1000:0]) a1<-mkReg(0);
Reg# (bit[1000:0]) a2<-mkReg(0);
Reg# (bit[1000:0]) a3<-mkReg(0);
Reg# (bit[1000:0]) mx<-mkReg(0);
Reg# (bit[1000:0]) qin<-mkReg(0);
Reg# (bit[29:0]) c<-mkReg(0);

Reg# (int) s<-mkReg(0);

Vector#(6 ,Sqr_ifc) sq;                // instantiating 6 squarer blocks
for(Integer i3=0;i3<6;i3=i3+1)  
	sq[i3]<-mk_sqr;
Quadcas_ifc qd <-mk_quadcas;           // instantiating the quad cascade block

rule cycle1((s==1)&&(|sq[2].result()!=0));  // quad of a0, a2 and a3 computed

sq[3].start(zeroExtend(sq[0].result()));
sq[4].start(zeroExtend(sq[1].result()));
sq[5].start(zeroExtend(sq[2].result()));
s<=2;
endrule
rule cycle2((s==2)&&(|sq[5].result()!=0));  // select the inputs for the multiplier by control signals
bit[1000:0] ma=0;
bit[1000:0] mb=0;
case (c[2:0])
	3'b000 : ma=a0;
	3'b001 : ma=zeroExtend(sq[0].result());
	3'b010 : ma=a2;
	3'b011 : ma=a0^a2;
	3'b100 : ma=zeroExtend(sq[3].result())^a1;
	3'b101 : ma=a1;
	3'b110 : ma=zeroExtend(sq[4].result());
endcase
case (c[5:3])
	3'b000 : mb=a1;
	3'b001 : mb=zeroExtend(sq[1].result());
	3'b010 : mb=zeroExtend(sq[1].result())^a2;
	3'b011 : mb=a1^a3;
	3'b100 : mb=zeroExtend(sq[2].result())^a1^a3;
	3'b101 : mb=a3;
	3'b110 : mb=zeroExtend(sq[5].result());
	3'b111 : mb=zeroExtend(sq[4].result());
endcase


bit[1000:0] ml=zeroExtend( hkmul233(ma[`nl:0] , mb[`nl:0])); // hybrid karatsuba function
bit[`nl:0] m =(ml[(`n*2):((`nl*2)+2-`m1)])^(ml[(`n*2):((`nl*2)+2-`m2)])^(ml[(`n*2):((`nl*2)+2-`m3)]);  // performing modulo on the output of multiplier
m = m^(ml[`nl:0])^(ml[(`nl*2):`n])^(ml[(`nl*2):`n]<<`m1)^(ml[(`nl*2):`n]<<`m2)^(ml[(`nl*2):`n]<<`m3);
ml=zeroExtend(m);
mx<=ml; // mx contains reduced output of multiplier
s<=4;
endrule


method Action start(a_0,a_1,a_2,a_3,q_in,cn);  // takes inputs A0, A1, A2  , Qin and cn is a 30 bit control signal)
a0<=a_0;
a1<=a_1;
a2<=a_2;
a3<=a_3;
qin<=q_in;
c<=cn;


qd.start2(q_in,c[29:26]); // quad cascade block takes input qin and c[29:26] is to choose the appropriate quad power)
sq[0].start(a_0);  // square of a0 , a1 and a2 computed
sq[1].start(a_1);
sq[2].start(a_2);

s<=1;

endmethod

method qout()if(s==4);            // output qout of quad cascade block

return (qd.result2()); 
endmethod
method c0()if(s==4);            // output c0 is muxed out

case (c[7:6])
	2'b00 : return(mx[`nl:0]);
	2'b01 : return(mx[`nl:0]^a2[`nl:0]);
	2'b10 : return(sq[1].result());
	2'b11 : return(mx[`nl:0]^a3[`nl:0]);
	
endcase
endmethod
method c1()if(s==4);            // output c1 is muxed out

case (c[9:8])
	2'b00 : return(mx[`nl:0]^a0[`nl:0]);
	2'b01 : return(mx[`nl:0]^a0[`nl:0]^sq[2].result());
	2'b10 : return(sq[4].result());
	2'b11 : return(sq[3].result());
	
endcase
endmethod
method status();           // returns status of ALU block
if(s==4)
	return(1'b1);
else
	return(1'b0);

endmethod

endmodule:mk_ALU

module mktestalu(); // testbench for alu
Reg#(int) state<-mkReg(0);
ALU_ifc mta<-mk_ALU;

rule go(state==0);
mta.start(zeroExtend(233'b11100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
),zeroExtend(233'b11100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
),zeroExtend(233'b11100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
),zeroExtend(233'b11100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
),zeroExtend(233'b11100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
),30'b000000000000000000001000110000); //(inputs : a0,a1,a2,a3,qin and a 30 bit control signal)
state<=2;
endrule

rule finish(state==2);
$display("qout=%b",mta.qout());
$display("c0=%b",mta.c0()); // c0
$display("c1=%b",mta.c1()); // c1
$display("status=%b",mta.status()); // status

state<=4;
endrule

endmodule:mktestalu

endpackage:ALU






