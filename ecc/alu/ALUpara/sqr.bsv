//----------------------- SQUARER BLOCK-------------------------
package sqr;
import Vector::*;
import sqr_func::*;
interface Sqr_ifc#(numeric type nx);//nx denotes the field used
method Action start(Bit#(nx) a); // input for quad block


method Bit#(nx) result();

endinterface: Sqr_ifc

module mk_sqr(Sqr_ifc#(nx))
provisos(Add#(0,n2,nx),  Add#(a__, nx, 600));

Reg# (Bit#(600)) b<-mkReg(0);

Reg# (Bit#(1500)) x<-mkReg(0);
Reg# (int) n<-mkReg(fromInteger(valueOf(nx)));
Reg# (int) s<-mkReg(0);

Reg#(int) i2<-mkReg(0);

rule add_Zero(s==1); // adding zeros between each bit of input
x[2*i2]<=b[i2];
i2<=i2+1;
if(i2==n)
	s<=2;
endrule
       





rule cyclce(s==2); // performing modulo to reduce the oupt to 233 bits
 
b<=sqr_fun(x,n);
s<=4;
endrule


method Action start(a);



b<=zeroExtend(a);
s<=1;
i2<=0;
endmethod


method result()if(s==4);
return (b[(n-1):0]); // output of the quad block
endmethod


endmodule:mk_sqr

module mktest(); //testbench
Reg#(int) state<-mkReg(0);
Sqr_ifc#(233) m<-mk_sqr;

rule go(state==0);
m.start(233'b01000000000010000000000000000000100000000101000000011101000000010100000000011000000100000000000010000000000000000001010000000000010000000001100000010000000001001000000000000000000000000000000100000000111100000010101000001110100000000
);         // change the input according to the field
state<=2;
endrule

rule finish(state==2);
$display("Quad=%b",m.result()); // output is displayed in  bits.

state<=4;
endrule

endmodule:mktest

endpackage:sqr
