
import random
import sys
import cocotb
import logging as log
from cocotb.decorators import coroutine
from cocotb.triggers import Timer, RisingEdge
from cocotb.monitors import BusMonitor
from cocotb.drivers import BusDriver
from cocotb.binary import BinaryValue
from cocotb.scoreboard import Scoreboard
from cocotb.result import TestFailure
from cocotb.clock import Clock
from pyrsa import mme, mmm, msb, rr


class Driver(BusDriver):
    """Drives inputs to DUT"""
    _signals = [
#
                'mmeExp_a1',
                'mmeExp_exponent1',
                'mmeExp_n1',
                'mmeExp_r2_mod_n1',
                'EN_mmeExp',
                ]
    def __init__(self, dut):
        BusDriver.__init__(self, dut, None, dut.CLK)
        
        
class InputTransaction(object):
    """Transactions sent to Driver"""
    def __init__(self, tb, 
#
        mmeExp_a1=0,
        mmeExp_exponent1=0,
        mmeExp_n1=0,
        mmeExp_r2_mod_n1=0,
        EN_mmeExp=1,
                ):
#
        self.mmeExp_a1 = BinaryValue(mmeExp_a1, tb.mmeExp_a1_bits, False)
        self.mmeExp_exponent1 = BinaryValue(mmeExp_exponent1, tb.mmeExp_exponent1_bits, False)
        self.mmeExp_n1 = BinaryValue(mmeExp_n1, tb.mmeExp_n1_bits, False)
        self.mmeExp_r2_mod_n1 = BinaryValue(mmeExp_r2_mod_n1, tb.mmeExp_r2_mod_n1_bits, False)
        self.EN_mmeExp = BinaryValue(EN_mmeExp, tb.EN_mmeExp_bits, False)
        

class InputMonitor(BusMonitor):
    """Passive input monitor of DUT"""
    _signals = [
#
		'mmeExp_a1',
		'mmeExp_exponent1',
		'mmeExp_n1',
		'mmeExp_r2_mod_n1',
		'EN_mmeExp',
                ]
    def __init__(self, dut, callback=None, event=None):
        BusMonitor.__init__(self, dut, None, 
                            dut.CLK, dut.RST_N,
                            callback=callback, event=event)
        self.name = "in"
    
    @coroutine
    def _monitor_recv(self):
        clkedge = RisingEdge(self.clock)
        EN_mmeExp_edge = RisingEdge(self.bus.EN_mmeExp)

        while True:
            yield EN_mmeExp_edge  # inputs are to be driven only when this signal is high
            en_bit = self.bus.EN_mmeExp.value
            if en_bit.integer == 1:
                                
                vec = (
                    self.bus.mmeExp_a1.value.integer,
                    self.bus.mmeExp_exponent1.value.integer,
                    self.bus.mmeExp_n1.value.integer,
                    self.bus.mmeExp_r2_mod_n1.value.integer,
                    self.bus.EN_mmeExp.value.integer,
                    )
                self._recv(vec)



class OutputTransaction(object):
    """ Transaction to be expected / received by OutputMonitor"""
    def __init__(self, tb=None, 
#
			RDY_mmeExp=1,
			getResult=0,
			RDY_getResult=1,
			isReady=1,
			RDY_isReady=1,
				):
        """For expected transactions, value 'None' means don't care.
        tb must be an instance of the Testbench class."""
#
        if RDY_mmeExp is not None and isinstance(RDY_mmeExp, int):
            RDY_mmeExp = BinaryValue(RDY_mmeExp, tb.RDY_mmeExp_bits, False)
        if getResult is not None and isinstance(getResult, int):
            getResult = BinaryValue(getResult, tb.getResult_bits, False)
        if RDY_getResult is not None and isinstance(RDY_getResult, int):
            RDY_getResult = BinaryValue(RDY_getResult, tb.RDY_getResult_bits, False)
        if isReady is not None and isinstance(isReady, int):
            isReady = BinaryValue(isReady, tb.isReady_bits, False)
        if RDY_isReady is not None and isinstance(RDY_isReady, int):
            RDY_isReady = BinaryValue(RDY_isReady, tb.RDY_isReady_bits, False)
        
#
        self.value = (
			RDY_mmeExp,
			getResult,
			RDY_getResult,
			isReady,
			RDY_isReady,
            )



class OutputMonitor(BusMonitor):
    """Observes signals of DUT"""
    _signals = [
#
            'RDY_mmeExp',
            'getResult',
            'RDY_getResult',
            'isReady',
            'RDY_isReady',
            'EN_mmeExp'
                ]
    
    def __init__(self, dut, tb):
        BusMonitor.__init__(self, dut, None, dut.CLK, dut.RST_N)
        self.name = "out"
        self.tb = tb

    @coroutine
    def _monitor_recv(self):
        clkedge = RisingEdge(self.clock)
        isReady_edge = RisingEdge(self.bus.isReady)  # DUT packet to be obtained only when this signal is high
        while True:
            yield isReady_edge
            en_bit = self.bus.EN_mmeExp.value
            if en_bit.integer ==1:
            	rdy_bit = self.bus.RDY_mmeExp.value
            	if rdy_bit.integer == 1:
                	

                	self._recv(OutputTransaction(self.tb,
                                             	self.bus.RDY_mmeExp.value,
                                             	hex(self.bus.getResult.value),
                                             	self.bus.RDY_getResult.value,
                                             	self.bus.isReady.value,
                                             	self.bus.RDY_isReady.value,

                                             	))



class DUTScoreboard(Scoreboard):
    def compare(self, got, exp, log, **_):
        
        got_output="".join(str(got.value))
        exp_output="".join(str(exp.value))
        log.info("\nExpected Output: {0}.\nGot Output: {1}.".format(exp_output, got_output))
        self.errors=0
        if got_output != exp_output:
            self.errors +=1
            log.error("ERROR Outputs differ .")
        else:
            log.info('PASSED Outputs match.')
            
            

class TestBench(object):
    """Verification Test Bench"""
    def __init__(self, dut):
        self.dut = dut
        self.stopped = False
        
        """Signal length"""
#
        self.mmeExp_a1_bits = 2048
        self.mmeExp_exponent1_bits = 2048
        self.mmeExp_n1_bits = 2048
        self.mmeExp_r2_mod_n1_bits = 2048
        self.EN_mmeExp_bits = 1
#
        self.RDY_mmeExp_bits = 1
        self.getResult_bits = 2048
        self.RDY_getResult_bits = 1
        self.isReady_bits = 1
        self.RDY_isReady_bits = 1
        
        self.input_drv = Driver(dut)

        self.input_mon = InputMonitor(dut)

        init_val = OutputTransaction(self)

        self.output_mon = OutputMonitor(dut, self)

        self.expected_output = []
        self.scoreboard = DUTScoreboard(dut)
        self.scoreboard.add_interface(self.output_mon, self.expected_output)

    def model(self, transaction):
        """Model"""
#
        (
            mmeExp_a1, 
            mmeExp_exponent1, 
            mmeExp_n1, 
            mmeExp_r2_mod_n1, 
            EN_mmeExp,  ) = transaction

        #RDY_mmeExp = 0
        getResult = 0
        #RDY_getResult = 0
        #isReady = 0
        #RDY_isReady = 0
        
        ndig = 2048
        
        a = 0x3EF8F0B6A84A7C9F60C53609493A6E5D97550553669A8FF328051AD8918A29A7C714E0F32DDC5C12097C53F50EAE335EEF73081E66D54AC2334C278E016417DD9E562BFA43B11E3913B923D6162F0D9D0779D53EE54C2CC7F7A57982DE24755209FCF142CC0062DC51F0567FDD21312CCEE613EFF40597E20980E8E2E7972067D771ED20A7DFCA15727B777A243E19335404A6DBB0FBAC64D73AC9F1436E40BED14745E6DFC26C8BB7813901CB1D9FF2FFE49D72C542E7DB928DF7E46F7CBB54C60862B1FFDEA43767956F1C8F46F0952E5BAA83FCA5ADB92C9C555E1160648F97B129E1FA121B20BFAA7631E79366E8A1211364E90AA492F820D620F7ECCEB8
        e = 0x10001ABC3579093BCDFBD894570423BDAC25749655465214761BDBCBABFB4578232E13B2FAABCBF5462789540BBCC0000136435161CBBBAAFF33113768EEE764751532908643876514BCDFEABCFACDB26576538662BDAECBDEAC3468900094999DDCCBBE9008776DEACBD4126374850FEBCCAE39475940CBDEBDEAEFBABFABDBDB7598657513967900000865714758162BDFEFCBAFDE78967345731BFCEAFEBCFEA876965514866666666666666617480000004BFCEFABDFE54637183945853793BFFEAFEBCFAE312648376492852367326534865789078945626BFEEEEECCBFBCFAECFEBFEAFC74756347289101928376EFABCD237849604732EFDBCAFE3285
        n = 0xDBC6686641E484AD414CAC014AA5735FE6BC4382438EDBDA4E0B0712E03D2E0058A64126828D0A9157B761B563469BA4388B06D4D3EF129DF72F98806C141E049D8A7AED75063BF585F6F26F04E2A6060C41FC3336FC0D6C316DBEF1DE4E16AE095F470404A15298089BCB9EFB6F71FB903374D9B2C1849BF805780278DFC49D2C801D0BB6EEDD7B3445BDF3D6F14BE26E408DBC2B65FFA9F74882929378E7F52C1837BD35AF9EF346CFB93E4ADBE96245BEBBF260416C82B2E6EC5073294D64DFAAF29084EDDCADE68BE1BBE4387F4EE0D1297B44F553EB03EBFA2589592F74ADCD29BB961AE1A32BCB00F5793F132B552CDE09A829CFF240A5F85EB30B9467
        rr = (2**(2*ndig))%n
        
        
        getResult = hex(int(pyrsa.mme(a,e,n,rr)))
        
        
        """ Model being called here """
        self.expected_output.append(OutputTransaction(self,
#
                    RDY_mmeExp,
                    getResult,
                    RDY_getResult,
                    isReady,
                    RDY_isReady,
                    
            ))

    def stop(self):
        """Stop generating input data.

        Also stop generation of expected output transactions.
        One more clock cycle must be executed afterwards so that the output of
        the D flip-flop can be checked.
        """
        
        self.stopped = True



def random_input_gen(tb, n=10000):
    for i in range(n):
        mmeExp_a1 = random.randint(0,
                                   0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)
        mmeExp_exponent1 = random.randint(0,
                                          0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)
        mmeExp_n1 = random.randint(0,
                                   0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)
        ndig = 2048
        mmeExp_r2_mod_n1 = (2 ** (2 * ndig)) % mmeExp_n1
        EN_mmeExp = 1

        yield InputTransaction(tb,
                               mmeExp_a1,
                               mmeExp_exponent1,
                               mmeExp_n1,
                               mmeExp_r2_mod_n1,
                               EN_mmeExp
                               )


@cocotb.coroutine
def clock_gen(signal):
    while True:
        signal <= 0
        yield Timer(1) 
        signal <= 1
        yield Timer(1)

@cocotb.test()
def run_test(dut):
    cocotb.fork(clock_gen(dut.CLK))
    tb = TestBench(dut)
    dut.RST_N <= 0
    yield Timer(2) 
    dut.RST_N <= 1
    input_gen = random_input_gen(tb)
    
    yield tb.input_drv.send(input_gen, False)

    for t in input_gen:
        yield tb.input_drv.send(t)

    yield tb.input_drv.send(InputTransaction(tb))
    tb.stop()
    yield RisingEdge(dut.CLK)

    raise tb.scoreboard.result














