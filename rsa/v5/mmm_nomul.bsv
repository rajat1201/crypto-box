package mmm_nomul;

  import FIFO::*;
  import GetPut::*;
  import LFSR::*;
  import bigadd_updated ::*;
  import Vector ::*;

  interface Ifc_mmm#(numeric type n, numeric type d);
    method Action ma_send_inp(Bit#(n) a, Bit#(n) b, Bit#(n) m);
    method ActionValue#(Bit#(n)) mav_get_result;
    method Bool isReady();
  endinterface

  module mkmmm(Ifc_mmm#(n,d))
    provisos(Add#(a__, TLog#(n), n),
            Add#(b__, TLog#(TAdd#(1, n)), TAdd#(TLog#(n), 1)), Mul#(c__, d, TAdd#(n, d)), Mul#(num_vec, d, n));

    Reg#(Bit#(TAdd#(n,1))) rg_S <- mkReg(0);
    Reg#(Bit#(n)) rg_A <- mkReg(0);
    Reg#(Bit#(n)) rg_B <- mkReg(0);
    Reg#(Bit#(n)) rg_M <- mkReg(0);
    Reg#(Bit#(n)) rg_M_neg <- mkReg(0);
    Reg#(Bit#(TAdd#(TLog#(n),1))) rg_iter <- mkReg(0);
    Reg#(Bool) rg_start <- mkReg(False);
    Reg#(Bool) rg_done <- mkReg(False);
    Reg#(Bit#(n)) counter <- mkReg(0);
    Reg#(Bit#(1)) rg_carry <- mkReg(0);

    Vector#(num_vec, Reg#(Bit#(d))) out <- replicateM(mkReg(0));

    let d_v = fromInteger(valueOf(d));
    

    Reg#(Bit#(TAdd#(n,1))) lv_S <- mkReg(0);

    Reg#(Int#(10)) rg_state <- mkReg(0);

    Ifc_bigadd#(d) ba_mod <- mkBigAdd;

    Reg#(Int#(32)) cl_cycle <- mkReg(0);

    rule rl_init_add(rg_state==1);
      $display("MMM: Start");
      if(rg_A[0]==1) begin
        if(counter<(fromInteger(valueOf(n)/valueOf(d)))) begin
          let lv_temp <- ba_mod.addn(rg_S[d_v*counter+d_v-1:d_v*counter], rg_B[d_v*counter+d_v-1:d_v*counter], rg_carry);
          out[counter] <= lv_temp[d_v-1:0];
          rg_carry <= lv_temp[d_v];
          counter<=counter+1;
        end
        else begin
          rg_state<=3;
          counter<=0;
          lv_S <= {rg_carry,pack(readVReg(out))};
        end
      end
      else begin
        rg_state<=3;
        lv_S <= rg_S;
      end
    endrule

    rule rl_next_ass(rg_state==3);
      $display("MMM: Next");
      if(lv_S[0]==0) begin
        lv_S <= (lv_S>>1);
        rg_state<=5;
      end
      else begin
        if(counter<(fromInteger(valueOf(n)/valueOf(d)))) begin
          let lv_temp <- ba_mod.addn(lv_S[d_v*counter+d_v-1:d_v*counter], rg_M[d_v*counter+d_v-1:d_v*counter], rg_carry);
          out[counter] <= lv_temp[d_v-1:0];
          rg_carry <= lv_temp[d_v];
          counter<=counter+1;
        end
        else begin
          rg_state<=5;
          counter<=0;
          lv_S <= ({rg_carry,pack(readVReg(out))})>>1;
        end
      end
    endrule

    rule final_check(rg_state==5);
      $display("MMM: Final %d", rg_iter);
      if(rg_iter==fromInteger(valueOf(n)-1)) begin
        rg_iter<=0;
        if(lv_S>=zeroExtend(rg_M)) begin
          if(counter<(fromInteger(valueOf(n)/valueOf(d)))) begin
            let lv_temp <- ba_mod.addn(lv_S[d_v*counter+d_v-1:d_v*counter], (rg_M_neg)[d_v*counter+d_v-1:d_v*counter],rg_carry);
            out[counter] <= lv_temp[d_v-1:0];
            rg_carry <= lv_temp[d_v];
            counter<=counter+1;
          end
          else begin
            rg_state<=9;
            counter<=0;
            rg_S <= ({rg_carry,pack(readVReg(out))});
          end
        end
        else begin
          rg_state<=9;
          rg_S<=lv_S;
        end
      end
      else begin
        rg_iter<=rg_iter+1;
        rg_state<=1;
        rg_S <= lv_S;
        rg_A <= rg_A>>1;
      end
    endrule

    method Action ma_send_inp(Bit#(n) a, Bit#(n) b, Bit#(n) m) if(rg_state==0);
      rg_A<= a;
      rg_B<= b;
      rg_M<= m;
      rg_M_neg<= (~m)|1;
      rg_S<= 0;
      rg_start<= True;
      rg_state<=1;
      counter<=0;
    endmethod

    method ActionValue#(Bit#(n)) mav_get_result if(rg_state==9);
      rg_start<= False;
      rg_done<= False;
      rg_state<=0;
      return truncate(rg_S);
    endmethod

    method Bool isReady();
      if(rg_state==0 || rg_state==9)
        return True;
      else
        return False;
    endmethod
  endmodule

  module mkTb(Empty);
    Ifc_mmm#(128, 64) mmm_inst <-mkmmm;

    rule rl_start;
      Bit#(128) lv_A= 'd94; //90,49 //Actual inp 34,23
      Bit#(128) lv_B= 'd22;
      Bit#(128) lv_M= 'd101;
      mmm_inst.ma_send_inp(lv_A, lv_B, lv_M);
      //mmm_inst.ma_send_inp('d53687093, 'd1073770090, 'd1610612799);
      //mmm_inst.ma_send_inp('d1181116071, 'd1, 'd1610612799);
      //mmm_inst.ma_send_inp('d763786781, 'd1, 'd1610612799);
      $display("Sending inputs A: %h B: %h M: %h", lv_A, lv_B, lv_M);
    endrule

    rule rl_get_result;
      let res<- mmm_inst.mav_get_result;
      $display($time,"\t Result: %d", res);
      $finish(0);
    endrule

  endmodule
endpackage
