package mme_impl;
  import FIFO::*;
  import GetPut::*;
  import LFSR::*;

  import mmm_nomul ::*;
  import "BDPI" function Bit#(32) golden_result (Bit#(32) a, Bit#(32) e, Bit#(32) m);

  interface Ifc_mme#(numeric type m);
    method Action mmeExp(Bit#(m) a1, Bit#(m) exponent1, Bit#(m) n1, Bit#(m) r2_mod_n1);
    method Bit#(m) getResult();
    method Bool isReady();
  endinterface
  typedef enum {
    Idle, FindExpLength, FindAMonty, StartExp, Compute, Compute1_25, Compute1_5, Compute2, Compute3, Compute4, Done
  } MME_State_type deriving(Bits, Eq, FShow);
  
  module mkMME(Ifc_mme#(m)) provisos(Add#(a__, TLog#(m), m),Add#(b__, TLog#(TAdd#(1, m)), TAdd#(TLog#(m), 1)),Add#(c__, m, TMul#(2, m)));//, Add#(0,m,32));
    Reg#(Bit#(m)) a <- mkReg(0);
    Reg#(Bit#(m)) a_montg <- mkReg(0);
    Reg#(Bit#(m)) exponent <- mkReg(0);
    Reg#(Bit#(m)) n <- mkReg(0);
    Reg#(Bit#(m)) r2_mod_n <- mkReg(0);
    Reg#(Bit#(m)) res <- mkReg(0);
    Reg#(Bit#(m)) res_temp <- mkReg(0);
    Reg#(Int#(TAdd#(TLog#(m),1))) i <- mkReg(0);
    Reg#(Bool) completed <- mkReg(False);
  
    Reg#(MME_State_type) rg_state <- mkReg(Idle);
    Ifc_mmm#(m) mod_m <- mkmmm;
   
    rule initialise(rg_state==FindExpLength);
      if(res!=0) begin
        i <= i+1;
        res <= res>>1;
      end
      else
        rg_state <= FindAMonty;
    endrule
    rule convert_A_To_Monty(rg_state==FindAMonty);
      mod_m.ma_send_inp(a,r2_mod_n,n);
      rg_state <= Compute;
    endrule
  
    rule compute((rg_state==Compute) && i>=0);
      let lv_t <- mod_m.mav_get_result();
      a_montg <= lv_t;
      res <= lv_t;
      rg_state <= Compute1_25;
    endrule
  
    rule loopDecr((rg_state==Compute1_25) && i>=0);
      i<=i-1;
      rg_state<=Compute1_5;
    endrule
  
    rule compute1_5 (rg_state==Compute1_5 && i>=0);
      //i<=i-1;
      mod_m.ma_send_inp(res,res,n);
      rg_state <= Compute2;
    endrule
  
    rule compute2(rg_state==Compute2 && i>=0);
      let lv_t <- mod_m.mav_get_result();
      res <= lv_t;
      if(exponent[i]==1)
        rg_state <= Compute3;
      else begin
        rg_state <= Compute1_25;
      end
    endrule
  
    rule compute3(rg_state==Compute3 && i>=0);
      mod_m.ma_send_inp(res,a_montg,n);
      rg_state <= Compute4;
    endrule
  
    rule compute4(rg_state==Compute4 && i>=0);
      let lv_t <- mod_m.mav_get_result();
      res <= lv_t;
      rg_state <= Compute1_25;
    endrule
    rule postProcess((rg_state==Compute1_25)||(rg_state==Compute1_5)||(rg_state==Compute2));
      mod_m.ma_send_inp(res,1,n);
      rg_state <= Done;
    endrule
  
    rule done(rg_state==Done && i<0);
      let lv_t <- mod_m.mav_get_result();
      res <= lv_t;
      rg_state <= Idle;
    endrule
  
    method Action mmeExp(Bit#(m) a1, Bit#(m) exponent1, Bit#(m) n1, Bit#(m) r2_mod_n1) if(rg_state==Idle);
      //$display("rsaExp %d, %d, %d, %d", a1, exponent1, n1, r2_mod_n1);
      a<=a1;
      exponent<=exponent1;
      n<=n1;
      r2_mod_n<=r2_mod_n1;
      res<=exponent1;
      rg_state<=FindExpLength;
      i<=-1;
    endmethod
  
    method Bit#(m) getResult() if(rg_state==Idle);
      if(res!=n)
        return res;
      else
        return 0;
    endmethod
  
    method Bool isReady();
      return (rg_state==Idle);
    endmethod
    
  endmodule
  
  module mkTb(Empty);
      Ifc_mme#(32) mme_mod <-mkMME;
      LFSR#(Bit#(32)) lfsr1 <- mkLFSR_32;
      LFSR#(Bit#(32)) lfsr2 <- mkLFSR_32;
      LFSR#(Bit#(32)) lfsr3 <- mkLFSR_32;
      Reg#(Bit#(32)) rg_A <- mkReg(1);
      Reg#(Bit#(32)) rg_E <- mkReg(1);
      Reg#(Bit#(32)) rg_M <- mkReg(1);
      Reg#(Bit#(29)) rg_counter <- mkReg(0);
      Reg#(Bool) rg_start <- mkReg(False);
      Reg#(Bool) rg_given_inputs <- mkReg(False);
      Reg#(Bit#(32)) rg_B <- mkReg(2);
      Reg#(Bit#(32)) rg_baseExp <- mkReg(64);
  
      rule rl_seed(!rg_start);
        rg_start<= True;
        lfsr1.seed('h10);
        lfsr2.seed('h9);
        lfsr3.seed('h8);
      endrule
  
      rule rl_start(rg_start && !rg_given_inputs);
        let lv_M= (lfsr3.value>>1 | 'd1);  //Always odd
        lfsr3.next;
        let lv_A= lfsr1.value % lv_M;   //Making sure that input 1 is lesser than M
        lfsr1.next;
        let lv_E= lfsr2.value % lv_M;   //Making sure that input 2 is lesser than M
        lfsr2.next;
       
        //Bit#(32) r2_mod_M= (2^64) % lv_M;
        //lv_M= 'd101;
        Bit#(32) r2_mod_M= golden_result(rg_B, rg_baseExp, lv_M);
        if(lv_A!=0 && lv_E!=0 && lv_A!=1) begin
          mme_mod.mmeExp(lv_A, lv_E, lv_M, r2_mod_M);
          //$display("A: %d E: %d M: %d r2_mod_M: %d", lv_A, lv_E, lv_M, r2_mod_M);
          rg_A<= lv_A;
          rg_E<= lv_E;
          rg_M<= lv_M;
          rg_given_inputs<= True;
        end
      endrule
  
      rule rl_get_result(mme_mod.isReady && rg_given_inputs);
        let res= mme_mod.getResult;
        //$display("\nNew output..");
        let expected_res= golden_result(rg_A, rg_E, rg_M);  // (rg_A ^ rg_E ) % rg_M;
        rg_counter<= rg_counter+1;
        rg_given_inputs<= False;
        if(expected_res != res) begin
          $display($time,"\t Mismatch!! Golden result: %d", expected_res);
          $display($time,"\t %d. A: %d E: %d M: %d Result:  %d", rg_counter, rg_A, rg_E, rg_M, res);
          $finish(0);
        end
        if(rg_counter=='d10000) begin
          $display("Checked %d testcases. MME works!", rg_counter);
          $finish(0);
        end
      endrule
  endmodule


endpackage
