package mmm_nomul;

  import FIFO::*;
  import GetPut::*;
  import LFSR::*;

  interface Ifc_mmm#(numeric type n);
    method Action ma_send_inp(Bit#(n) a, Bit#(n) b, Bit#(n) m);
    method ActionValue#(Bit#(n)) mav_get_result;
  endinterface

  module mkmmm(Ifc_mmm#(n))
    provisos(Add#(a__, TLog#(n), n),
            Add#(b__, TLog#(TAdd#(1, n)), TAdd#(TLog#(n), 1)));
    Reg#(Bit#(TAdd#(n,1))) rg_S <- mkReg(0);
    Reg#(Bit#(n)) rg_A <- mkReg(0);
    Reg#(Bit#(n)) rg_B <- mkReg(0);
    Reg#(Bit#(n)) rg_M <- mkReg(0);
    Reg#(Bit#(TAdd#(TLog#(n),1))) rg_iter <- mkReg(0);
    Reg#(Bool) rg_start <- mkReg(False);
    Reg#(Bool) rg_done <- mkReg(False);

    rule rl_exe(rg_start && !rg_done);
      Bit#(TAdd#(n,1)) lv_S= rg_S;
      if(rg_A[0]==1) begin
        lv_S= lv_S + zeroExtend(rg_B);
      end
      if(lv_S[0]==0) begin
        lv_S= lv_S >> 1;
      end
      else begin
        lv_S= (lv_S + zeroExtend(rg_M)) >> 1;
      end
      
      //$display($time,"\t Iter: %d A: %d R: %d", rg_iter, rg_A, rg_S);
      if(rg_iter==fromInteger(valueOf(n)-1)) begin
        rg_done<= True;
        rg_iter<= 0;
        if(lv_S>zeroExtend(rg_M)) begin
          lv_S= lv_S - zeroExtend(rg_M);
          //$display("This happened");
        end
      end
      else begin
        rg_iter<= rg_iter + 1;
      end
      rg_A<= rg_A >> 1;
      rg_S<= lv_S;

    endrule

    method Action ma_send_inp(Bit#(n) a, Bit#(n) b, Bit#(n) m) if(!rg_start);
      //$display("MMM: i/p A: %d, B: %d, M:%d", a, b, m);
      rg_A<= a;
      rg_B<= b;
      rg_M<= m;
      rg_start<= True;
    endmethod

    method ActionValue#(Bit#(n)) mav_get_result if(rg_start && rg_done);
      //$display("MMM: o/p: %d", rg_S);
      rg_start<= False;
      rg_done<= False;
      rg_S<= 0;
      return truncate(rg_S);
    endmethod
  endmodule

  module mkTb(Empty);
    Ifc_mmm#(32) mmm_inst <-mkmmm;

    rule rl_start;
      Bit#(32) lv_A= 'd49; //90,49 //Actual inp 34,23
      Bit#(32) lv_B= 'd1;
      Bit#(32) lv_M= 'd101;
      //mmm_inst.ma_send_inp(lv_A, lv_B, lv_M);
      mmm_inst.ma_send_inp('d53687093, 'd1073770090, 'd1610612799);
      //mmm_inst.ma_send_inp('d1181116071, 'd1, 'd1610612799);
      //mmm_inst.ma_send_inp('d763786781, 'd1, 'd1610612799);
      $display($time,"\t Sending inputs A: %d B: %d M: %d", lv_A, lv_B, lv_M);
    endrule

    rule rl_get_result;
      let res<- mmm_inst.mav_get_result;
      $display($time,"\t Result: %d", res);
      $finish(0);
    endrule

  endmodule
endpackage
