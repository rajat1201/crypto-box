package mmm;

import Vector ::*;

interface Ifc_mmm#(numeric type m);
  method Action ma_send_inp(Bit#(m) a1, Bit#(m) b1, Bit#(m) n1);
  method Bool isReady();
  method Bit#(m) mav_get_result();
endinterface

module mkmmm(Ifc_mmm#(m))provisos(Add#(a__, m, TMul#(2, m)));
  Reg#(Bit#(m)) rg_P <- mkReg(0);
  Reg#(Bit#(m)) rg_R <- mkReg(0);
  Reg#(Bit#(m)) rg_A <- mkReg(0);
  Reg#(Bit#(m)) rg_B <- mkReg(0);
  Reg#(Bit#(m)) rg_N <- mkReg(0);
  Reg#(Bit#(TMul#(2,m))) rg_temp <- mkReg(0);

  Reg#(Int#(3)) rg_state <- mkReg(0);
  Reg#(Int#(TAdd#(TLog#(m),2))) i <- mkReg(0);

  rule initialse(rg_state==1 && i<fromInteger(valueOf(m)));
    rg_temp <= zeroExtend(rg_A*rg_B);
    rg_state <= 2;
  endrule

  rule compute2(rg_state==2 && i<fromInteger(valueOf(m)));
      if(rg_temp[i] == 1 && (rg_P+1)[0] == 1)
        rg_P <= (rg_P+rg_N+1)>>1;
      else if(rg_temp[i] == 1 && (rg_P+1)[0] == 0)
        rg_P <= (rg_P+1)>>1;
      else if(rg_temp[i] == 0 && rg_P[0] == 1)
        rg_P <= (rg_P+rg_N)>>1;
      else
        rg_P <= rg_P>>1;
      i <= i+1;
  endrule

  rule done(rg_state!=0 && i==fromInteger(valueOf(m)));
    rg_R <= rg_P+rg_temp[fromInteger(2*valueOf(m)-1):fromInteger(valueOf(m))];
    i <= 0;
    rg_state <= 0;
  endrule

  method Action ma_send_inp(Bit#(m) a1, Bit#(m) b1, Bit#(m) n1);
    rg_A<=a1;
    rg_B<=b1;
    rg_N<=n1;
    rg_P<=0;
    i<=0;
    rg_state<=1;
  endmethod

  method Bool isReady();
    return (rg_state==0);
  endmethod

  method Bit#(m) mav_get_result();
    return rg_R;
  endmethod
endmodule

endpackage
