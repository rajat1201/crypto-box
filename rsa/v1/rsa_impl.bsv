package rsa_impl;
import mmm ::*;

interface Ifc_rsa#(numeric type m);
  method Action rsaExp(Bit#(m) a1, Bit#(m) exponent1, Bit#(m) n1, Bit#(m) r2_mod_n1);
  method Bit#(m) getResult();
  method Bool isReady();
endinterface
typedef enum {
  Idle, FindExpLength, FindAMonty, StartExp, Compute, Compute1_25, Compute1_5, Compute2, Compute3, Compute4, Done
} RSA_State_type deriving(Bits, Eq, FShow);

module mkRSA(Ifc_rsa#(m)) provisos(Add#(a__, TLog#(m), m),Add#(b__, TLog#(TAdd#(1, m)), TAdd#(TLog#(m), 1)),Add#(c__, m, TMul#(2, m)));//, Add#(0,m,32));
  Reg#(Bit#(m)) a <- mkReg(0);
  Reg#(Bit#(m)) a_montg <- mkReg(0);
  Reg#(Bit#(m)) exponent <- mkReg(0);
  Reg#(Bit#(m)) n <- mkReg(0);
  Reg#(Bit#(m)) r2_mod_n <- mkReg(0);
  Reg#(Bit#(m)) res <- mkReg(0);
  Reg#(Bit#(m)) res_temp <- mkReg(0);
  Reg#(Int#(TAdd#(TLog#(m),1))) i <- mkReg(0);
  Reg#(Bool) completed <- mkReg(False);

  Reg#(RSA_State_type) rg_state <- mkReg(Idle);
  Ifc_mmm#(m) mod_m <- mkmmm;
 
  rule initialise(rg_state==FindExpLength);
    if(res!=0) begin
      i <= i+1;
      res <= res>>1;
      $display("i %h", i);
    end
    else
      rg_state <= FindAMonty;
  endrule
  rule convert_A_To_Monty((rg_state==FindAMonty) && mod_m.isReady);
    mod_m.ma_send_inp(a,r2_mod_n,n);
    rg_state <= Compute;
  endrule
 
  rule compute((rg_state==Compute) && i>=0 && mod_m.isReady);
    //let lv_t <- mod_m.mav_get_result();
    //a_montg <= lv_t;
    //res <= lv_t;
    a_montg <= mod_m.mav_get_result();
    res <= mod_m.mav_get_result();
    rg_state <= Compute1_25;
  endrule

  rule loopDecr((rg_state==Compute1_25) && i>=0);
    i<=i-1;
    rg_state<=Compute1_5;
  endrule

  rule compute1_5 (rg_state==Compute1_5 && i>=0 && mod_m.isReady);
    //i<=i-1;
    mod_m.ma_send_inp(res,res,n);
    rg_state <= Compute2;
    $display("Hi1_5");
  endrule

  rule compute2(rg_state==Compute2 && i>=0 && mod_m.isReady);
    ///let lv_t <- mod_m.mav_get_result();
    //res <= lv_t;
    res <= mod_m.mav_get_result();
    if(exponent[i]==1)
      rg_state <= Compute3;
    else begin
      rg_state <= Compute1_25;
    end
  endrule

  rule compute3(rg_state==Compute3 && i>=0);
    mod_m.ma_send_inp(res,a_montg,n);
    rg_state <= Compute4;
    $display("Compute3");
  endrule

  rule compute4(rg_state==Compute4 && i>=0 && mod_m.isReady);
    //let lv_t <- mod_m.mav_get_result();
    //res <= lv_t;
    res <= mod_m.mav_get_result();
    rg_state <= Compute1_25;
  endrule
  rule postProcess(((rg_state==Compute1_25)||(rg_state==Compute1_5)||(rg_state==Compute2)) && i<0 && mod_m.isReady);
    mod_m.ma_send_inp(res,1,n);
    rg_state <= Done;
    $display("Hi");
  endrule

  rule done(rg_state==Done && mod_m.isReady() && i<0);
    //let lv_t <- mod_m.mav_get_result();
    //res <= lv_t;
    res <= mod_m.mav_get_result();
    rg_state <= Idle;
  endrule

  method Action rsaExp(Bit#(m) a1, Bit#(m) exponent1, Bit#(m) n1, Bit#(m) r2_mod_n1) if(rg_state==Idle);
    a<=a1;
    exponent<=exponent1;
    n<=n1;
    r2_mod_n<=r2_mod_n1;
    $display("rsaExp %h, %h, %h, %h", a1, exponent1, n1, r2_mod_n1);
    res<=exponent1;
    rg_state<=FindExpLength;
    i<=-1;
  endmethod

  method Bit#(m) getResult() if(rg_state==Idle);
    if(res!=n)
      return res;
    else
      return 0;
  endmethod

  method Bool isReady();
    return (rg_state==Idle);
  endmethod
  
endmodule

module mkTb(Empty);
    Ifc_rsa#(32) rsa_mod <-mkRSA;
    Reg#(Bool) rdy <- mkReg(False);
    rule rl_start(!rdy);
      //rsa_mod.rsaExp(1024'd23, 1024'd10, 1024'd101, 1024'd25);
      rsa_mod.rsaExp(32'd23, 32'd10, 32'd101, 32'd79);
      rdy <= True;
    endrule

    rule rl_get_result (rdy);
      if(rsa_mod.isReady) begin
        $display("Result:  %d", rsa_mod.getResult);
        $finish(0);
      end
    endrule
endmodule


endpackage
